package com.dy.shiro.service;

import com.alibaba.fastjson.JSONObject;

/**
 * @author huangdeyao
 * @date 2019/7/11 15:44
 */
public interface PermissionService {
    /**
     * 查询某用户的 角色  菜单列表   权限列表
     *
     * @param username
     * @return
     */
    JSONObject getUserPermission(String username);
}

package com.dy.shiro.service;

import com.alibaba.fastjson.JSONObject;
import com.dy.shiro.Entity.UserEntity;

/**
 * @author huangdeyao
 * @date 2019/7/11 16:05
 */
public interface UserService {
    /**
     * 查询用户列表
     *
     * @param pageNo
     * @param pagePage
     * @return
     */
    JSONObject listUser(int pageNo, int pagePage);

    /**
     *
     * @param userEntity
     * @return
     */
    JSONObject addUser(UserEntity userEntity);

    JSONObject updateUser(UserEntity userEntity);
}

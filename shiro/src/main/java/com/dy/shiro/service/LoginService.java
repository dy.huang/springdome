package com.dy.shiro.service;

import com.alibaba.fastjson.JSONObject;

/**
 * @author huangdeyao
 * @date 2019/7/11 12:02
 * 登录Service
 */
public interface LoginService {

    /**
     * 根据用户名和密码查询对应的用户
     *
     * @param username 用户名
     * @param password 密码
     * @return
     */
    JSONObject getUser(String username, String password);

    /**
     * 登录表单提交
     *
     * @param requestJson
     * @return
     */
    JSONObject authLogin(JSONObject requestJson);

    /**
     * 查询当前登录用户的权限等信息
     *
     * @return
     */
    JSONObject getInfo();
}

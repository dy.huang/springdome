package com.dy.shiro.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.dy.shiro.Entity.UserEntity;
import com.dy.shiro.constants.ErrorEnum;
import com.dy.shiro.dao.UserDao;
import com.dy.shiro.service.UserService;
import com.dy.shiro.utils.CommonUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author huangdeyao
 * @date 2019/7/11 16:05
 */
@Service
@Log4j2
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public JSONObject listUser(int pageNo, int pagePage) {
        int count = userDao.countUser();
        List<JSONObject> list = userDao.listUser(pageNo, pagePage);

        JSONObject returnData = new JSONObject();
        returnData.put("list", list);
        returnData.put("totalCount", pageNo);
        returnData.put("totalPage", count);
        return returnData;
    }

    @Override
    public JSONObject addUser(UserEntity userEntity) {
        int exist = userDao.queryExistUsername(userEntity.getUsername());
        log.info("exist:", exist);
        if (exist > 0) {
            return CommonUtil.errorJson(ErrorEnum.E_10009);
        }
        userDao.addUser(userEntity);
        return CommonUtil.successJson();
    }

    @Override
    public JSONObject updateUser(UserEntity userEntity) {
        userDao.updateUser(userEntity);
        return CommonUtil.successJson();
    }
}

package com.dy.shiro.controllers;

import com.alibaba.fastjson.JSONObject;
import com.dy.shiro.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author huangdeyao
 * @date 2019/7/11 12:25
 * 登录相关Controller
 */
@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    /**
     * 登录
     *
     * @param requestJson
     * @return
     */
    @PostMapping("/auth")
    public JSONObject authLogin(@RequestBody JSONObject requestJson) {
        return loginService.authLogin(requestJson);
    }

    /**
     * 查询当前登录用户的信息
     *
     * @return
     */
    @GetMapping("/getInfo")
    public JSONObject getInfo() {
        return loginService.getInfo();
    }

}

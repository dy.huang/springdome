package com.dy.shiro.controllers;

import com.alibaba.fastjson.JSONObject;
import com.dy.shiro.Entity.UserEntity;
import com.dy.shiro.service.UserService;
import com.dy.shiro.utils.CommonUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author huangdeyao
 * @date 2019/7/11 16:05
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 查询用户列表
     *
     * @param pageNo
     * @param pagePage
     * @return
     */
    @RequiresPermissions("user:list")
    @GetMapping("/list")
    public JSONObject listUser(@RequestParam(defaultValue = "0") int pageNo, @RequestParam(defaultValue = "10") int pagePage) {
        return userService.listUser(pageNo, pagePage);
    }

    /**
     * 新增用户
     * @param userEntity
     * @return
     */
    @RequiresPermissions("user:add")
    @PostMapping("/addUser")
    public JSONObject addUser(@RequestBody UserEntity userEntity) {
        return userService.addUser(userEntity);
    }

    @RequiresPermissions("user:update")
    @PostMapping("/updateUser")
    public JSONObject updateUser(@RequestBody UserEntity userEntity) {
        return userService.updateUser(userEntity);
    }
}

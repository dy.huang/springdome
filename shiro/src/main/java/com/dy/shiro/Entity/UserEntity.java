package com.dy.shiro.Entity;

import lombok.Data;

/**
 * @author huangdeyao
 * @date 2019/7/11 16:34
 */
@Data
public class UserEntity {
    private String username;
    private String password;
    private String nickname;
    private String roleId;
    private String userId;
}

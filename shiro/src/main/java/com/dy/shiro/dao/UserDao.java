package com.dy.shiro.dao;

import com.alibaba.fastjson.JSONObject;
import com.dy.shiro.Entity.UserEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author huangdeyao
 * @date 2019/7/11 16:08
 */
@Repository
public interface UserDao {
    /**
     * 查询用户数量
     *
     * @return
     */
    int countUser();

    /**
     * 查询用户列表
     *
     * @param pageNo
     * @param pagePage
     * @return
     */
    List<JSONObject> listUser(@Param("offSet") int pageNo, @Param("pageRow") int pagePage);

    /***
     * 查询表中是否存在数据
     * @param username
     * @return
     */
    int queryExistUsername(@Param("username") String username);

    /**
     * 添加用户
     *
     * @param userEntity
     */
    void addUser(UserEntity userEntity);

    void updateUser(UserEntity userEntity);
}

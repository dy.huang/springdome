package com.example.demo3.interceptor;


import com.alibaba.fastjson2.JSONObject;
import com.example.demo3.data.Message;
import com.example.demo3.data.MessageKey;
import org.springframework.web.socket.*;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 实现消息处理器WebSocketHandler
 * <p>
 * 要点解析：
 * <p>
 * 使用sUserMap这个静态变量来保存用户信息。对应我们上述的关系图。
 * 实现afterConnectionEstablished方法，当用户进入房间成功后，保存用户信息到Map，并调用sendMessageToRoomUsers广播新人加入信息。
 * 实现handleMessage方法，处理用户加入，离开和发送信息三类消息。
 * 实现afterConnectionClosed方法，用来处理当用户离开房间后的信息销毁工作。从Map中清除该用户。
 * 实现sendMessageToUser、sendMessageToRoomUsers两个向客户端发送消息的方法。直接通过Session即可发送结构化数据到客户端。sendMessageToUser实现了点对点消息的发送，sendMessageToRoomUsers实现了广播消息的发送。
 */
public class MyHandler implements WebSocketHandler {
    //用来保存用户、房间、会话三者。使用双层Map实现对应关系。
    private static final Map<String, Map<String, WebSocketSession>> sUserMap = new HashMap<>(3);

    //用户加入房间后，会调用此方法，我们在这个节点，向其他用户发送有用户加入的通知消息。
    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        System.out.println("成功建立连接");
        String INFO = session.getUri().getPath().split("INFO=")[1];
        System.out.println(INFO);
        if (INFO != null && INFO.length() > 0) {
            JSONObject jsonObject = JSONObject.parseObject(INFO);
            String command = jsonObject.getString("command");
            String roomId = jsonObject.getString("roomId");
            if (MessageKey.ENTER_COMMAND.equals(command)) {
                Map<String, WebSocketSession> mapSession = sUserMap.computeIfAbsent(roomId, k -> new HashMap<>(3));
                mapSession.put(jsonObject.getString("name"), session);
                session.sendMessage(new TextMessage("当前房间在线人数" + mapSession.size() + "人"));
                System.out.println(session);
            }
        }
        System.out.println("当前在线人数：" + sUserMap.size());
    }

    //消息处理方法
    @Override
    public void handleMessage(WebSocketSession webSocketSession, WebSocketMessage<?> webSocketMessage) {
        try {
            JSONObject jsonobject = JSONObject.parseObject(webSocketMessage.getPayload().toString());
            Message message = new Message(jsonobject.toString());
            System.out.println(jsonobject);
            System.out.println(message + ":来自" + webSocketSession.getAttributes().get(MessageKey.KEY_WEBSOCKET_USERNAME) + "的消息");
            if (message.getName() != null && message.getCommand() != null) {
                switch (message.getCommand()) {
                    case MessageKey.ENTER_COMMAND:
                        sendMessageToRoomUsers(message.getRoomId(), new TextMessage("【" + getNameFromSession(webSocketSession) + "】加入了房间，欢迎！"));
                        break;
                    case MessageKey.MESSAGE_COMMAND:
                        if (message.getName().equals("all")) {
                            sendMessageToRoomUsers(message.getRoomId(), new TextMessage(getNameFromSession(webSocketSession) + "说：" + message.getInfo()
                            ));
                        } else {
                            sendMessageToUser(getNameFromSession(webSocketSession), message.getRoomId(), message.getName(),
                                    new TextMessage(getNameFromSession(webSocketSession) + "悄悄对你说：" + message.getInfo()));
                        }
                        break;
                    case MessageKey.LEAVE_COMMAND:
                        sendMessageToRoomUsers(message.getRoomId(), new TextMessage("【" + getNameFromSession(webSocketSession) + "】离开了房间，欢迎下次再来"));
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 发送信息给指定用户
     *
     * @param name
     * @param message
     * @return
     */
    public boolean sendMessageToUser(String sender, String roomId, String name, TextMessage message) {
        if (roomId == null || name == null) return false;
        if (sUserMap.get(roomId) == null) return false;
        WebSocketSession session = sUserMap.get(roomId).get(name);
        if (null == session) {
            WebSocketSession senderSession = sUserMap.get(roomId).get(sender);
            TextMessage textMessage = new TextMessage(name + "不在线");
            try {
                senderSession.sendMessage(textMessage);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }
        if (!session.isOpen()) return false;
        try {
            session.sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 广播信息
     *
     * @param message
     * @return
     */
    public boolean sendMessageToRoomUsers(String roomId, TextMessage message) {
        if (roomId == null) return false;
        if (sUserMap.get(roomId) == null) return false;
        boolean allSendSuccess = true;
        Collection<WebSocketSession> sessions = sUserMap.get(roomId).values();
        for (WebSocketSession session : sessions) {
            try {
                if (session.isOpen()) {
                    session.sendMessage(message);
                }
            } catch (IOException e) {
                e.printStackTrace();
                allSendSuccess = false;
            }
        }

        return allSendSuccess;
    }

    @Override
    public void handleTransportError(WebSocketSession webSocketSession, Throwable throwable) throws Exception {
        System.out.println("连接出错");
        if (webSocketSession.isOpen()) {
            webSocketSession.close();
        }
        Map<String, WebSocketSession> map = sUserMap.get(getRoomIdFromSession(webSocketSession));
        if (map != null) {
            map.remove(getNameFromSession(webSocketSession));
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession webSocketSession, CloseStatus closeStatus) {
        System.out.println("连接已关闭：" + closeStatus);
        Map<String, WebSocketSession> map = sUserMap.get(getRoomIdFromSession(webSocketSession));
        if (map != null) {
            map.remove(getNameFromSession(webSocketSession));
        }
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }

    /**
     * 获取用户名称
     *
     * @param session
     * @return
     */
    private String getNameFromSession(WebSocketSession session) {
        try {
            String name = (String) session.getAttributes().get(MessageKey.KEY_WEBSOCKET_USERNAME);
            return name;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取房间号
     *
     * @param session
     * @return
     */
    private String getRoomIdFromSession(WebSocketSession session) {
        try {
            String roomId = (String) session.getAttributes().get(MessageKey.KEY_ROOM_ID);
            return roomId;
        } catch (Exception e) {
            return null;
        }
    }

}

package com.example.demo3.data;


import com.alibaba.fastjson2.JSONObject;

public class Message implements MessageKey {
    private String roomId;
    private String name;
    private String command;
    private String info;

    public Message(String json) {
        if (json != null && json.length() > 0) {
            JSONObject jsonObject = JSONObject.parseObject(json);
            this.roomId = jsonObject.get(KEY_ROOM_ID) != null ? jsonObject.get(KEY_ROOM_ID).toString() : null;
            this.command = jsonObject.get(KEY_MESSAGE_COMMAND) != null ? jsonObject.get(KEY_MESSAGE_COMMAND).toString() : null;
            this.info = jsonObject.get(KEY_INFO) != null ? jsonObject.get(KEY_INFO).toString() : null;
            this.name = jsonObject.get(KEY_WEBSOCKET_USERNAME) != null ? jsonObject.get(KEY_WEBSOCKET_USERNAME).toString() : null;
        }
    }

    public JSONObject toJson() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(KEY_INFO, this.info);
        jsonObject.put(KEY_WEBSOCKET_USERNAME, this.name);
        jsonObject.put(KEY_MESSAGE_COMMAND, this.command);
        jsonObject.put(KEY_ROOM_ID, this.roomId);
        return jsonObject;
    }

    public String getRoomId() {
        return roomId;
    }

    public String getName() {
        return name;
    }

    public String getCommand() {
        return command;
    }

    public String getInfo() {
        return info;
    }
}

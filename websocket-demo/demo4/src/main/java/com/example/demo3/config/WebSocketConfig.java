package com.example.demo3.config;

import com.example.demo3.interceptor.MyHandler;
import com.example.demo3.interceptor.WebSocketInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {
    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
//        注册WebSocketHandler（MyHandler），这是用来处理WebSocket建立以及消息处理的类，后面会详细介绍。
//注册WebSocketInterceptor拦截器，此拦截器用来在客户端向服务端发起初次连接时，记录客户端拦截信息。
//注册WebSocket地址，并附带了{INFO}参数，用来注册的时候携带用户信息。

        registry.addHandler(new MyHandler(), "/webSocket/{INFO}")
                .setAllowedOrigins("*")
                .addInterceptors(new WebSocketInterceptor());
    }
}

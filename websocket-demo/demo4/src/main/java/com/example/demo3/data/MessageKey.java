package com.example.demo3.data;

public interface MessageKey {
    /**
     * 进入
     */
    String ENTER_COMMAND = "enter";
    /**
     * 离开
     */
    String LEAVE_COMMAND = "leave";
    /**
     * 信息
     */
    String MESSAGE_COMMAND = "message";
    /**
     * 需要执行的操作
     */
    String KEY_MESSAGE_COMMAND = "command";
    /**
     * 信息
     */
    String KEY_INFO = "info";
    /**
     * 用户名
     */
    String KEY_WEBSOCKET_USERNAME = "name";
    /**
     * 房间号
     */
    String KEY_ROOM_ID = "roomId";
}

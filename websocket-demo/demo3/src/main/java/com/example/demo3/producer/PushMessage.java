package com.example.demo3.producer;

import com.alibaba.fastjson.JSON;
import com.example.demo3.data.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
 
/**
 * @Author JCccc
 * @Description   消息发送
 * @Date 2021/6/30 8:53
 */
@Service
public class PushMessage {
    @Autowired
    private SimpMessagingTemplate template;
 
    public void send(String msgJson){
 
        System.out.println("步骤2.即将推送给websocket server：");
 
        Message message = JSON.parseObject(msgJson, Message.class);
 
        System.out.println("步骤2.1 消息发给："+message.getTo());
        System.out.println("步骤2.2 发送内容是："+message.getContent());
        template.convertAndSendToUser(message.getTo(), "/message", message.getContent());
        System.out.println("----------消息发送完毕----------");
        
        //广播
       // template.convertAndSend("/topic/public",chatMessage);
    }
}
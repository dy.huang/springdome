package com.example.demo3.listener;

import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
 
/**
 * @Author JCccc
 * @Description   redis监听消息
 * @Date 2021/6/30 8:53
 */
public class RedisListener implements MessageListener {
 
    @Override
    public void onMessage(Message message, byte[] bytes) {
        System.out.println("步骤1.监听到需要进行负载转发的消息：" + message.toString());
        InjectServiceUtil.getInstance().pushMessage().send(message.toString());
    }
}
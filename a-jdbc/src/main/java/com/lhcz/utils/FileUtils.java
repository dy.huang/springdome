package com.lhcz.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * @author hdy
 */
public class FileUtils {


    protected String renderSqlByFile(String tplName, HashMap<String, Object> data) throws IOException {
        return renderSqlByString(getSql(tplName), data);
    }

    protected String renderSqlByString(String sqlStr, HashMap<String, Object> data) {
        return null;
    }

    /**
     * 从resources/sql文件夹获取sql文件
     *
     * @param filename
     * @return
     * @throws IOException
     */
    public static String getSql(String filename) {
        String ret;
        try {
            ret = FileUtils.readStringFromResource("/sql/" + filename + ".sql");
        } catch (Exception e) {
            ret = e.getLocalizedMessage();
        }
        return ret;
    }

    public static String readStringFromResource(String f) {
        if (!f.startsWith("/")) {
            f = "/" + f;
        }
        InputStream is = FileUtils.class.getResourceAsStream(f);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String s = "";
        StringBuilder sb = new StringBuilder();
        try {
            while ((s = br.readLine()) != null) {
                sb.append(s).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}

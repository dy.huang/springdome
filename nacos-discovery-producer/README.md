# Spring Cloud + Nacos 三部曲之Discovery服务注册发现

1、[Spring Cloud+Nacos 三部曲之Config](https://gitee.com/dy.huang/springdome/tree/master/nacos-config)

2、[Spring Cloud + Nacos 三部曲之Discovery服务注册发现](https://gitee.com/dy.huang/springdome/tree/master/nacos-discovery-producer)

3、[Spring Cloud + Nacos 三部曲之Discovery消费者](https://gitee.com/dy.huang/springdome/tree/master/nacos-discovery-client)

##  版本
1. springboot版本：2.1.6.RELEASE
2. nacos版本[Nacos 1.1.0](https://github.com/alibaba/nacos/releases)

## 创建一个springboot项目
#### 快速开始
1. pom引用
```xml
        <!--配置 这里配置config，后面测试有用-->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
            <version>0.9.0.RELEASE</version>
        </dependency>
        <!--服务注册-->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
            <version>0.9.0.RELEASE</version>
        </dependency>
```
2. yml配置

修改配置名称： bootstrap.yml
添加配置信息
```yml
spring:
  application:
    name: nacos-producer
  cloud:
    nacos:
      config:
        server-addr: 127.0.0.1:8848
        file-extension: yaml
      discovery:
        server-addr: 127.0.0.1:8848
server:
  port: 8899

```

3. 代码

```java

@SpringBootApplication
@EnableDiscoveryClient
public class NacosDiscoveryProducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(NacosDiscoveryProducerApplication.class, args);
    }

    @RestController
    public class EchoController {
        @GetMapping(value = "/echo/{string}")
        public String echo(@PathVariable String string) {
            return "Hello Nacos Discovery " + string;
        }
    }
}

```

启动程序，查看nacos管理页面，在服务列表可以查看到当前应用

4. 在配置里添加服务地址和基础配置测试
 在配置列表添加(格式： YAML)
 Data ID:  nacos-producer.yaml
 Group:  DEFAULT_GROUP
 ```
 spring:
  application:
    name: nacos-producer
  cloud:
    nacos:
      config:
        server-addr: 127.0.0.1:8848
        file-extension: yaml
      discovery:
        server-addr: 127.0.0.1:8848
server:
  port: 8899
 ```

 发布，启动程序，看到端口改变，服务列表显示注册成功






[代码](https://gitee.com/dy.huang/springdome/tree/master/nacos-discovery-producer)

[个人站点](https://www.yrclubs.com)

reference
----

[Nacos Config Example](https://github.com/alibaba/spring-cloud-alibaba/blob/master/spring-cloud-alibaba-examples/nacos-example/nacos-config-example/readme-zh.md)

[Spring Cloud Alibaba Nacos Config](https://github.com/alibaba/spring-cloud-alibaba/blob/master/spring-cloud-alibaba-docs/src/main/asciidoc-zh/nacos-config.adoc)

[Spring Cloud Alibaba Nacos Discovery](https://github.com/alibaba/spring-cloud-alibaba/blob/3c6a71cac3c2d85429a0d1d53c587a2716e05831/spring-cloud-alibaba-docs/src/main/asciidoc-zh/nacos-discovery.adoc)


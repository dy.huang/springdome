package com.dy;

import lombok.extern.log4j.Log4j2;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.rest.RestStatus;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

//@Ignore
@Log4j2
@RunWith(SpringRunner.class)
@SpringBootTest
public class ElasticsearchApplicationTests {

    @Autowired
    RestHighLevelClient restHighLevelClient;

    @Test
    public void contextLoads() {

        for (int i = 10; i >= 0; i--) {
            Map<String, Object> jsonMap = new HashMap<>();
            jsonMap.put("name", "测试" + i);
            jsonMap.put("age", "" + i);
            jsonMap.put("des", "啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦拉了拉");
            jsonMap.put("des1", "des1des1des1des1des1des1des1des1des1des1des1des1des1des1des1des1des1des1des1des1des1des1des1");
            jsonMap.put("des2", "des2des2des2des2des2des2des2des2des2des2des2des2des2des2des2des2des2des2");
            jsonMap.put("des3", "des3des3des3des3des3des3des3des3des3des3des3des3des3des3des3des3des3des3");
            jsonMap.put("des4", "des4des4des4des4des4des4des4des4des4des4des4des4des4des4des4des4des4des4des4des4des4des4des4des4des4");
            jsonMap.put("postDate", new Date());
            jsonMap.put("message", "trying out Elasticsearch");
            IndexRequest indexRequest = new IndexRequest("test").id("" + i).source(jsonMap);
            try {
                IndexResponse response = null;
                try {
                    response = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                log.info(response.toString());
            } catch (ElasticsearchException e) {
                if (e.status() == RestStatus.CONFLICT) {
                    System.out.println("e = " + e);
                }
            }
        }
//
//        GetRequest getRequest = new GetRequest("posts", "2");
//        GetResponse response = null;
//        try {
//            response = restHighLevelClient.get(getRequest, RequestOptions.DEFAULT);
//            log.info(response.toString());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }

}

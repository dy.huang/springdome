package com.dy.service.service;

import com.dy.model.Log;
import com.dy.service.service.facedata.dto.LogQueryCriteria;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.scheduling.annotation.Async;

import java.io.IOException;

/**
 *
 */
public interface LogService {

    /**
     * 分页查询
     *
     * @param criteria 查询条件
     * @return /
     */
    Object queryAll(LogQueryCriteria criteria) throws IOException;


    /**
     * 保存日志数据
     *
     * @param username  用户
     * @param browser   浏览器
     * @param ip        请求IP
     * @param joinPoint /
     * @param log       日志实体
     */
    @Async
    void save(String username, String browser, String ip, ProceedingJoinPoint joinPoint, Log log) throws IOException;


    /**
     * 保存日志数据
     *
     * @param username    用户
     * @param jobCode     组织机构
     * @param ip          ip
     * @param method      方法
     * @param description 描述
     * @param logType     类型
     */
    @Async
    void saveLogin(String username, String jobCode, String ip, String method, String description, String logType);

    /**
     * 查询异常详情
     *
     * @param id 日志ID
     * @return Object
     */
    Object findByErrDetail(Long id);

    /**
     * 删除所有错误日志
     */
    void delAllByError();

    /**
     * 删除所有INFO日志
     */
    void delAllByInfo();
}

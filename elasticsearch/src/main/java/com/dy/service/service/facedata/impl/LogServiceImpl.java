package com.dy.service.service.facedata.impl;


import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.dy.aop.log.ALog;
import com.dy.model.Log;
import com.dy.service.service.LogService;
import com.dy.service.service.facedata.dto.LogEntity;
import com.dy.service.service.facedata.dto.LogQueryCriteria;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class LogServiceImpl implements LogService {

    private static final String DATE_TIME = "yyyy-MM-dd HH:mm:ss";
    private final RestHighLevelClient restHighLevelClient;

    @Autowired
    public LogServiceImpl(RestHighLevelClient restHighLevelClient) {
        this.restHighLevelClient = restHighLevelClient;
    }


    @Override
    public Object queryAll(LogQueryCriteria criteria) throws IOException {
//  开始时间:
//结束时间:
//用户名:
//组织机构:
//操作:
        SearchRequest searchRequest = new SearchRequest("sys_log");
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.from(0);
        sourceBuilder.size(10);
        sourceBuilder.sort(new FieldSortBuilder("createDate").order(SortOrder.DESC));
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        if (StringUtils.isNotBlank(criteria.getDescription())) {
            queryBuilder.must(QueryBuilders.wildcardQuery("description", "*" + criteria.getDescription() + "*"));
        }
        if (StringUtils.isNotBlank(criteria.getParams())) {
            queryBuilder.must(QueryBuilders.wildcardQuery("params", "*" + criteria.getParams() + "*"));
        }
        if (StringUtils.isNotBlank(criteria.getUsername())) {
            queryBuilder.must(QueryBuilders.termQuery("username", criteria.getUsername()));
        }
        if (StringUtils.isNotBlank(criteria.getRequestIp())) {
            queryBuilder.must(QueryBuilders.termQuery("requestIp", criteria.getRequestIp()));
        }
        if (StringUtils.isNotBlank(criteria.getStartTime())) {
            // gte >=
            queryBuilder.filter(QueryBuilders.rangeQuery("createDate").gte(criteria.getStartTime()));
        }
        if (StringUtils.isNotBlank(criteria.getEndTime())) {
            // lte <=
            queryBuilder.filter(QueryBuilders.rangeQuery("createDate").lte(criteria.getEndTime()));
        }

        sourceBuilder.query(queryBuilder);
        System.out.println("queryBuilder = " + queryBuilder);
        searchRequest.source(sourceBuilder);
        System.out.println("searchRequest = " + searchRequest);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHit[] searchHits = searchResponse.getHits().getHits();
        List<LogEntity> logEntities = new ArrayList<>();
        for (SearchHit hit : searchHits) {
            String sourceAsString = hit.getSourceAsString();
            LogEntity log = JSONObject.parseObject(sourceAsString, LogEntity.class);
            logEntities.add(log);
        }

        return logEntities;
    }

    @Override
    public void save(String username, String browser, String ip, ProceedingJoinPoint joinPoint, Log log) throws IOException {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        ALog aopLog = method.getAnnotation(ALog.class);
        // 方法路径
        String methodName = joinPoint.getTarget().getClass().getName() + "." + signature.getName() + "()";
        // 描述
        if (log != null) {
            log.setDescription(aopLog.value());
        }
        assert log != null;
        log.setRequestIp(ip);
        log.setAddress("内网");
        log.setMethod(methodName);
        log.setUsername(username);
        log.setParams(JSONObject.toJSONString(joinPoint.getArgs()));
        log.setBrowser(browser);
        log.setCreateDate(new Date());
        IndexRequest request = new IndexRequest("sys_log");
        request.id();


        String str = JSONObject.toJSONStringWithDateFormat(log, DATE_TIME);

        System.out.println("str = " + str);
        request.source(str, XContentType.JSON);
        restHighLevelClient.index(request, RequestOptions.DEFAULT);
    }

    @Override
    public Object findByErrDetail(Long id) {
        return null;
    }

    @Override
    public void saveLogin(String username, String jobCode, String ip, String method, String description, String logType) {
    }

    @Override
    public void delAllByError() {
    }

    @Override
    public void delAllByInfo() {
    }

//    PUT test_index
//{
//  "mappings": {
//    "properties": {
//      "TimeData": {
//        "type": "date",
//        "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
//      }
//    }
//  }
//}
//
//
//
//GET test/_search
//
//GET test/_mapping
//
//
//PUT test_index/_doc/6
//{
//  "TimeData":"2020-12-01 12:10:29",
//  "name":"t1"
//}
//
//GET _cat/indices
//
//GET test_index/_search
//{
//  "query": {
//    "range": {
//      "TimeData": {
//        "gte": "2020-12-01 12:10:11"
//      }
//    }
//  }
//}

}

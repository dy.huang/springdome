package com.dy.client;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;

/**
 * @author hdy
 */
@Configuration
public class EsConfiguration {
    /**
     * // 集群地址，多个用,隔开
     */
//    private static String hosts = "192.168.43.128";
//    private static String hosts1 = "192.168.43.129";
//    private static String hosts2 = "192.168.43.130";
    private static String hosts = "127.0.0.1";
    /**
     * 使用的端口号
     */
    private static int port = 9200;
    /**
     * // 使用的协议
     */
    private static String schema = "http";
    private static ArrayList<HttpHost> hostList = null;
    /**
     * 连接超时时间
     */
    private static int connectTimeOut = 1000;
    /**
     * 连接超时时间
     */
    private static int socketTimeOut = 30000;
    /**
     * 获取连接的超时时间
     */
    private static int connectionRequestTimeOut = 500;
    /**
     * 最大连接数
     */
    private static int maxConnectNum = 100;
    /**
     * 最大路由连接数
     */
    private static int maxConnectPerRoute = 100;

    private RestClientBuilder builder;

    private final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();

    static {
        hostList = new ArrayList<>();
        hostList.add(new HttpHost(hosts, port, schema));
//        hostList.add(new HttpHost(hosts1, port, schema));
//        hostList.add(new HttpHost(hosts2, port, schema));
    }

    @Bean("restHighLevelClient")
    public RestHighLevelClient client() {
        builder = RestClient.builder(hostList.toArray(new HttpHost[0]));
        setConnectTimeOutConfig();
        setMutiConnectConfig();
        return new RestHighLevelClient(builder);
    }

    /**
     * 异步httpclient的连接延时配置
     */
    private void setConnectTimeOutConfig() {
        builder.setRequestConfigCallback(requestConfigBuilder -> {
            requestConfigBuilder.setConnectTimeout(connectTimeOut);
            requestConfigBuilder.setSocketTimeout(socketTimeOut);
            requestConfigBuilder.setConnectionRequestTimeout(connectionRequestTimeOut);
            return requestConfigBuilder;
        });
    }

    /**
     * 异步httpclient的连接数配置
     */
    private void setMutiConnectConfig() {
//        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("elastic", "123456"));
        builder.setHttpClientConfigCallback(httpClientBuilder -> {
//            httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
            httpClientBuilder.setMaxConnTotal(maxConnectNum);
            httpClientBuilder.setMaxConnPerRoute(maxConnectPerRoute);
            return httpClientBuilder;
        });
    }

}

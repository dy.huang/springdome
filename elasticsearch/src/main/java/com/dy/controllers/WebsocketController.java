package com.dy.controllers;


import com.dy.aop.log.ALog;
import com.dy.service.service.facedata.dto.LogQueryCriteria;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @author dy.huang
 * @date 2020/5/10 23:54
 */
@RestController
@RequestMapping("/api/ws")
public class WebsocketController {

    /**
     * 发送信息
     *
     * @return /
     */
    @ALog
    @GetMapping("/send")
    public ResponseEntity<Object> send() {
        return new ResponseEntity<>("1232321", HttpStatus.OK);
    }


    /**
     * 查询岗位 code
     *
     * @param code /
     * @return /
     */
    @ALog("通过code 查询岗位")
    @GetMapping("/{code}")
    public ResponseEntity<Object> findByCode(@PathVariable String code) {
        return new ResponseEntity<>("1232321", HttpStatus.OK);
    }

    /**
     * 查询全部岗位
     *
     * @param criteria /
     * @return /
     */
    @ALog("查询岗位")
    @GetMapping
    public ResponseEntity<Object> getJobs(LogQueryCriteria criteria) {
        return new ResponseEntity<>("1232321", HttpStatus.OK);
    }

    /**
     * 新增岗位
     *
     * @param resources /
     * @return /
     */
    @ALog("新增岗位")
    @PostMapping
    public ResponseEntity<Object> create(@Validated @RequestBody LogQueryCriteria resources) {
        return new ResponseEntity<>("1232321", HttpStatus.OK);
    }

    /**
     * 修改岗位
     *
     * @param resources /
     * @return /
     */
    @ALog("修改岗位")
    @PutMapping
    public ResponseEntity<Object> update(@RequestBody LogQueryCriteria resources) {
        return new ResponseEntity<>("1232321", HttpStatus.OK);
    }

    /**
     * 删除岗位
     *
     * @param ids /
     * @return /
     */
    @ALog("删除岗位")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Set<Long> ids) {
        return new ResponseEntity<>("1232321", HttpStatus.OK);
    }

    /**
     * 通过jobcode 获区code列表
     *
     * @param code /
     * @return ；
     */
    @ALog("刪除")
    @DeleteMapping("/{code}")
    public ResponseEntity<Object> getAllByCode(@PathVariable String code) {
        return new ResponseEntity<>("1232321", HttpStatus.OK);
    }

    /**
     * 获取全部的组织机构 （组织机构）
     *
     * @return /
     */
    @ALog("查询全部组织机构")
    @GetMapping("/all")
    public ResponseEntity<Object> getAll() {
        return new ResponseEntity<>("1232321", HttpStatus.OK);
    }


}

import Vue from 'vue'
import Vuex from 'vuex'

import user from './modules/user'
import settings from './modules/settings'
import getters from './getters'
import permission from './modules/permission'
import app from './modules/app'
import tagsView from './modules/tagsView'
import api from './modules/api'

Vue.use(Vuex)
const store = new Vuex.Store({
  modules: {
    app,
    api,
    user,
    tagsView,
    settings,
    permission
  },
  getters
})

export default store

package com.dy;

import com.dy.service.AsyncService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.Async;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FtpOutApplicationTests {

    @Autowired
    private AsyncService asyncService;

    @Test
    public void contextLoads() throws ExecutionException, InterruptedException {
        int count = 10000;
        for (int i = 0; i < count; i++) {
            CompletableFuture<String> createOrder = asyncService.doSomething("index = " + i);
            System.out.println("createOrder = " + createOrder.get());
        }

        CompletableFuture<String> createOrder = asyncService.doSomething1("create order");
        CompletableFuture<String> reduceAccount = asyncService.doSomething2("reduce account");
        CompletableFuture<String> saveLog = asyncService.doSomething3("save log");

        // 等待所有任务都执行完
        CompletableFuture.allOf(createOrder, reduceAccount, saveLog).join();
        // 获取每个任务的返回结果
        String result = createOrder.get() + reduceAccount.get() + saveLog.get();
        System.out.println("result = " + result);
    }

}

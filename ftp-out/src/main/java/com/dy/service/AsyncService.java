package com.dy.service;

import org.springframework.scheduling.annotation.Async;

import java.util.concurrent.CompletableFuture;

/**
 * @author dy.huang
 * @date 2020/10/21 23:46
 */
public interface AsyncService {
    /**
     * pp
     *
     * @param message /
     * @return /
     */
    @Async
    CompletableFuture<String> doSomething(String message);

    @Async
    CompletableFuture<String> doSomething1(String message) throws InterruptedException;

    @Async
    CompletableFuture<String> doSomething2(String message) throws InterruptedException;

    @Async
    CompletableFuture<String> doSomething3(String message) throws InterruptedException;
}

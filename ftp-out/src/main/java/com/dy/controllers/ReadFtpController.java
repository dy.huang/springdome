package com.dy.controllers;

import com.dy.service.AsyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;

/**
 * @author dy.huang
 * @date 2021/1/17 22:21
 */
@RestController
@RequestMapping("api/read")
public class ReadFtpController {

    @Autowired
    private AsyncService asyncService;

    @GetMapping
    public String test() throws InterruptedException {
        int n = 0;
        while (true) {
            if (n == 10) {
                return "back";
            }
            Thread.sleep(1000);
            n++;
        }
    }

    @GetMapping("test")
    public CompletableFuture<String> test1() throws InterruptedException {
        return asyncService.doSomething("tes1t");
    }
}

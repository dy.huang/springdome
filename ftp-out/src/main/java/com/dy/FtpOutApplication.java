package com.dy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class FtpOutApplication {

    public static void main(String[] args) {
        SpringApplication.run(FtpOutApplication.class, args);
    }

}

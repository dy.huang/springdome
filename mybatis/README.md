# springdome2.0 + mybatis

#### 介绍
个人springdome 测试

#### 软件架构
mybatis

#### 1、导入表结构

```sql
create table test.user
(
    id bigint not null AUTO_INCREMENT comment '主键' primary key,
    age int null comment '年龄',
    password varchar(32) null comment '密码',
    sex int null comment '性别',
    username varchar(32) null comment '用户名'
);
```

#### 2、pom包

```xml

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>2.0.1</version>
        </dependency>

        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-jdbc</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

```
#### 3、yml配置

```yml
server:
  port: 8081

spring:
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    password: adminroot
    username: root
    url: jdbc:mysql://localhost:3306/vue_authentication?useUnicode=true&characterEncoding=utf-8&useSSL=true&serverTimezone=UTC

mybatis:
  mapper-locations: classpath:mapper/*.xml
  
```

#### 4、编写对应的实体类
>路径 com.dy.mybatis.entity.User
```java

package com.dy.mybatis.entity;

import lombok.Data;

/**
 * @author huangdeyao
 */
@Data
public class User {


    private Long id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 性别 1=男 2=女 其他=保密
     */
    private Integer sex;

}

```

#### 5、编写mapper接口
>路径 com.dy.mybatis.repository.UserMapper
```java
/**
 * @author huangdeyao
 */
@Service
@Mapper
public interface UserMapper {

    /**
     * 新增用户
     *
     * @param user
     * @return
     */
    int save(User user);

    /**
     * 更新用户信息
     *
     * @param user
     * @return
     */
    int update(User user);

    /**
     * 根据id删除
     *
     * @param id
     * @return
     */
    int deleteById(int id);

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    User selectById(int id);

    /**
     * 查询所有用户信息
     *
     * @return
     */
    List<User> selectAll();
}

```
#### 6、UserMapper.xml
>路径 resources/mapper/UserMapper.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.dy.mybatis.repository.UserMapper">

    <resultMap id="SysUserResultMap" type="com.dy.mybatis.entity.User">
        <id property="id" column="id" javaType="java.lang.Long" jdbcType="BIGINT"/>
        <result property="username" column="USERNAME" javaType="java.lang.String" jdbcType="VARCHAR"/>
        <result property="password" column="PASSWORD" javaType="java.lang.String" jdbcType="VARCHAR"/>
        <result property="sex" column="SEX" javaType="java.lang.Integer" jdbcType="INTEGER"/>
        <result property="age" column="AGE" javaType="java.lang.Integer" jdbcType="INTEGER"/>
    </resultMap>
    <delete id="deleteById">
        delete from user where id=#{id}
    </delete>

    <select id="selectAll" resultMap="SysUserResultMap">
        select * from user
    </select>


    <insert id="save" parameterType="com.dy.mybatis.entity.User">
        insert into user
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <if test="id != null">
                id,
            </if>
            <if test="username != null">
                username,
            </if>
            <if test="password != null">
                password,
            </if>

            <if test="sex != null">
                sex,
            </if>
            <if test="age != null">
                age,
            </if>
        </trim>
        <trim prefix="values (" suffix=")" suffixOverrides=",">
            <if test="id != null">
                #{id,jdbcType=BIGINT},
            </if>
            <if test="username != null">
                #{username,jdbcType=VARCHAR},
            </if>
            <if test="password != null">
                #{password,jdbcType=VARCHAR},
            </if>
            <if test="sex != null">
                #{sex,jdbcType=INTEGER},
            </if>
            <if test="age != null">
                #{age,jdbcType=INTEGER},
            </if>
        </trim>
    </insert>


    <update id="update" parameterType="com.dy.mybatis.entity.User">
        update user
        <set>
            <if test="username != null">
                username = #{username,jdbcType=VARCHAR},
            </if>
            <if test="password != null">
                password = #{password,jdbcType=VARCHAR},
            </if>
            <if test="sex != null">
                sex = #{sex,jdbcType=INTEGER},
            </if>
            <if test="age != null">
                sex = #{age,jdbcType=INTEGER},
            </if>
        </set>
        where id = #{id,jdbcType=BIGINT}
    </update>


    <select id="selectById" resultMap="SysUserResultMap">
        select
        *
        from user
        where id = #{id,jdbcType=BIGINT}
    </select>
</mapper>


```

#### 7、controller 测试
```java
package com.dy.mybatis.controllers;

import com.dy.mybatis.entity.User;
import com.dy.mybatis.repository.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author huangdeyao
 * @date 2019/7/8 16:29
 */
@RestController
public class MybatisController {

    @Autowired
    private UserMapper userMapper;

    @RequestMapping("/save")
    public int save() {
        User user = new User();
        user.setUsername("zzzz");
        user.setPassword("bbbb");
        user.setSex(1);
        user.setAge(18);
        // 返回插入的记录数 ，期望是1条 如果实际不是一条则抛出异常
        return userMapper.save(user);
    }

    @RequestMapping("/update")
    public int update() {
        User user = new User();
        user.setId(1L);
        user.setPassword("newpassword");
        // 返回更新的记录数 ，期望是1条 如果实际不是一条则抛出异常
        return userMapper.update(user);
    }

    @GetMapping("/find")
    public User selectById() {
        return userMapper.selectById(1);
    }

    @GetMapping("/delete")
    public int deleteById() {
        return userMapper.deleteById(1);
    }

    @GetMapping("/all")
    public List<User> all() {
        return userMapper.selectAll();
    }
}


```

#### 注意版本 
> 1、版本有出入也会有问题；pom里面的版本 boot 2.1.6, mybatis 2.0.1

> 2、 注意parameterType中的路径，yml中的配置路径

[码云](https://gitee.com/dy.huang/springdome)


reference
---
[【SpringBoot2.0系列05】SpringBoot之整合Mybatis](https://www.jianshu.com/p/c44dc639cb93) 

[基于 SpringBoot2.0+优雅整合 SpringBoot+Mybatis](https://segmentfault.com/a/1190000017211657)

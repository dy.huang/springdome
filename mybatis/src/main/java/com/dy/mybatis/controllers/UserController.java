package com.dy.mybatis.controllers;

import com.dy.mybatis.entity.User;
import com.dy.mybatis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author yukong
 * @date 2018/8/20 15:27
 * @description user控制器
 */
@RequestMapping("/user")
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public User save(@RequestBody User user) {
        return userService.saveUser(user);
    }

    @PutMapping
    public User update(@RequestBody User user) {
        return userService.updateUser(user);
    }

    @DeleteMapping(value = "/id/{id}")
    public Boolean delete(@PathVariable int id) {
        return userService.removeUser(id);
    }

    @GetMapping(value = "/id/{id}")
    public User findById(@PathVariable int id) {
        return userService.getById(id);
    }

}

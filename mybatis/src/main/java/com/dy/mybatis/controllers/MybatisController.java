package com.dy.mybatis.controllers;

import com.dy.mybatis.entity.User;
import com.dy.mybatis.repository.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author huangdeyao
 * @date 2019/7/8 16:29
 */
@RestController
public class MybatisController {

    @Autowired
    private UserMapper userMapper;

    @RequestMapping("/save")
    public int save() {
        User user = new User();
        user.setUsername("zzzz");
        user.setPassword("bbbb");
        user.setSex(1);
        user.setAge(18);
        // 返回插入的记录数 ，期望是1条 如果实际不是一条则抛出异常
        return userMapper.save(user);
    }

    @RequestMapping("/update")
    public int update() {
        User user = new User();
        user.setId(1L);
        user.setPassword("newpassword");
        // 返回更新的记录数 ，期望是1条 如果实际不是一条则抛出异常
        return userMapper.update(user);
    }

    @GetMapping("/find")
    public User selectById() {
        return userMapper.selectById(1);
    }

    @GetMapping("/delete")
    public int deleteById() {
        return userMapper.deleteById(1);
    }

    @GetMapping("/all")
    public List<User> all() {
        return userMapper.selectAll();
    }
}

package com.dy.mybatis.entity;

import lombok.Data;

/**
 * @author huangdeyao
 */
@Data
public class User {


    private Long id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 性别 1=男 2=女 其他=保密
     */
    private Integer sex;

}

package com.dy.mybatis.service;


import com.dy.mybatis.entity.User;

/**
 * @author huangdeyao
 * user业务层接口
 */
public interface UserService {

    /**
     * 新增用户
     *
     * @param user 用户信息
     * @return
     */
    User saveUser(User user);

    /**
     * 根据id删除用户
     *
     * @param id 主键id
     * @return
     */
    Boolean removeUser(int id);

    /**
     * 通过id查询用户
     *
     * @param id 主键id
     * @return
     */
    User getById(int id);

    /**
     * 根据主键id更新用户信息
     *
     * @param user 用户信息
     * @return
     */
    User updateUser(User user);

}

package com.dy.dockerdome.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author huangdeyao
 * @date 2019/8/29 13:48
 */
@RestController
public class DockerController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${spring.datasource.password}")
    private String password;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.url}")
    private String url;

    @GetMapping("/hello")
    public Object getEnv() {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("password", password);
        map.put("username", username);
        map.put("url", url);
        logger.info("========================");
        logger.info("{}", map);
        logger.info("=========*******************************=========");
        logger.info("", map);
        logger.info("========================");
        return map;
    }
}

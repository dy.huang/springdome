# dockercompose 读取springboot 动态配置 

- springboot中的yml配置

>${DATASOURCE_PASSWORD:123456*} ， 如果DATASOURCE_PASSWORD 没有值，默认123456*

```yml
server:
  tomcat:
    basedir: /home/temp
spring:
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    password: ${DATASOURCE_PASSWORD:123456*}
    username: ${DATASOURCE_USERNAME:root}
    url: ${DATASOURCE_URL:jdbc:mysql://localhost:3306/oauth2?useUnicode=true&characterEncoding=utf-8&useSSL=true&serverTimezone=UTC}
  jpa:
    database: mysql
    hibernate:
      ddl-auto: update
logging:
  path: /logs
```

- docker-compose.yml 配置（mysql+springdome）

```yml
version: '2'
services:      
  mysql:
    image: registry.cn-shanghai.aliyuncs.com/dy_docker/mysql:5.7
    restart: on-failure
    container_name: mysql
    ports:
      - 3306:3306
    environment: 
       TZ: Asia/Shanghai
       MYSQL_ROOT_PASSWORD: 123456*
    volumes:
      - ./mysql/mysqld.cnf:/etc/mysql/mysql.conf.d/mysqld.cnf
      - ./mysql/data:/var/lib/mysql
    networks:
      - my-network
  springdome:
    image: springdome:1.0
    restart: always
    container_name: springdome
    ports: 
      - "8081:8080"
    networks:
      - my-network
    volumes:
      - ./spring/tmp:/home/temp
      - ./spring/logs:/logs
    depends_on:
      - mysql
    environment:
      - DATASOURCE_PASSWORD=123456*
      - DATASOURCE_USERNAME=root
      - DATASOURCE_URL=jdbc:mysql://mysql:3306/oauth2?useUnicode=true&characterEncoding=utf-8&useSSL=true&serverTimezone=UTC
networks:
  my-network:
    driver: bridge
```

- 文件目录结构

```shell
[root@localhost docker-test]# tree
.
├── docker-compose.yml
├── dockerdome-0.0.1-SNAPSHOT.jar
├── Dockerfile
├── mysql
│   ├── data
│   └── mysqld.cnf
└── spring
    ├── logs
    └── tmp
```

- mysqld.cnf 配置文章（这个有点复杂 可以简单设置一下）

  ```yaml
  [mysqld_safe]
  socket          = /var/run/mysqld/mysqld.sock
  nice            = 0
  [mysqld]
  #
  # * Basic Settings
  #
  user            = mysql
  pid-file        = /var/run/mysqld/mysqld.pid
  socket          = /var/run/mysqld/mysqld.sock
  port            = 3306
  basedir         = /usr
  datadir         = /var/lib/mysql
  tmpdir          = /tmp
  lc-messages-dir = /usr/share/mysql
  skip-external-locking
  bind-address            = 0.0.0.0
  key_buffer_size         = 16M
  max_allowed_packet      = 16M
  thread_stack            = 192K
  thread_cache_size       = 8
  myisam-recover-options  = BACKUP
  query_cache_limit       = 1M
  query_cache_size        = 16M
  log_error = /var/log/mysql/error.log
  server-id               = 1
  log_bin                 = /var/log/mysql/mysql-bin
  binlog-format=ROW
  expire_logs_days        = 10
  max_binlog_size   = 100M
  ```

  - 项目地址[dockerdome](https://gitee.com/dy.huang/springdome/tree/master/dockerdome)
  - Dockerfile (这里的java包有点大，没有找到jre的，网络原因，有些资源下载不了)

  ```she
  FROM registry.cn-shanghai.aliyuncs.com/dy_docker/java:1.8
  
  MAINTAINER huangdeyao
  
  VOLUME /tmp
  
  ADD dockerdome-0.0.1-SNAPSHOT.jar /dockerdome.jar
  
  EXPOSE 8080
  
  ENTRYPOINT ["java", "-jar", "dockerdome.jar"]
  ```

  - 运行

  ```shell
  // 编译docker 镜像
  # docker build -t springdome:1.0 .
  // 后台运行
  # docker-compose up -d
  // 查看实时打印信息
  # tail -f spring/logs/spring.log 
  // 测试请求
  # curl http://192.168.177.129:8081/hello
  
  其他命令：
  # docker-compose ps
  # docker-compose stop springdome
  # docker-compose ps
  # docker-compose rm springdome
  # docker-compose images
  # docker mages
  # docker images
  # docker rmi b3e16f780660
  # docker images
  ```

reference

-------

[Springboot使用docker-compose实现动态配置](https://blog.csdn.net/xiaoguangtouqiang/article/details/90732394)

[用 Docker 构建、运行、发布来一个 Spring Boot 应用](https://blog.csdn.net/kkkloveyou/article/details/50942275)


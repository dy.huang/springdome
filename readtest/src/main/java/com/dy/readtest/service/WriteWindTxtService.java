package com.dy.readtest.service;

import com.dy.readtest.client.WriteTxtClient;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author huangdeyao
 * @date 2019/8/26 18:39
 */
public class WriteWindTxtService {

    @Autowired
    WriteTxtClient writeTxtClient;

    /**
     * 写入txt
     */
    private void writerHandle(String string) {
        try {
            writeTxtClient.getRw().write(string);
            writeTxtClient.getRw().newLine();
        } catch (Exception e) {
            System.err.println("write errors :" + e);
        }
    }


}

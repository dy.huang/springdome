package com.dy.readtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author huangdeyao
 */
@SpringBootApplication
public class ReadtestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReadtestApplication.class, args);
    }

}

package com.dy.readtest.client;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import sun.java2d.DisposerRecord;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * @author huangdeyao
 * @date 2019/8/26 18:09
 */
@Component
public class WriteTxtClient implements DisposerRecord {

    /**
     * 写入数据地址
     */
    private BufferedWriter bw;
    /**
     *
     */
    private static final String WEND_TXT_PATH = "C://data_input//value_map.txt";

    @Bean
    public BufferedWriter getRw() {
        try {
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(WEND_TXT_PATH)), StandardCharsets.UTF_8));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return bw;
    }

    @Override
    public void dispose() {
        if (bw != null) {
            try {
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

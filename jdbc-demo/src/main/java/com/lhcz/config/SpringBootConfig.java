package com.lhcz.config;

import com.lhcz.jfinal.kit.StrKit;
import com.lhcz.jfinal.template.Engine;
import com.lhcz.jfinal.template.ext.spring.JFinalViewResolver;
import com.lhcz.helper.JdbcHelper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * @author hdy
 */
@Configuration
public class SpringBootConfig {

    @Bean(name = "jfinalViewResolver")
    public JFinalViewResolver getJFinalViewResolver() {
        // 创建用于整合 spring boot 的 ViewResolver 扩展对象
        JFinalViewResolver jfr = new JFinalViewResolver();
        // 获取 engine 对象，对 enjoy 模板引擎进行配置，配置方式与前面章节完全一样
        Engine engine = JFinalViewResolver.engine;
        // 上面的代码获取到了用于 sql 管理功能的 Engine 对象，接着就可以开始配置了
        engine.addSharedMethod(new StrKit());
        // 热加载配置能对后续配置产生影响，需要放在最前面
        engine.setDevMode(true);
        // 使用 ClassPathSourceFactory 从 class path 与 jar 包中加载模板文件
        engine.setToClassPathSourceFactory();
        //指定默认的日期格式化样式
        jfr.setDatePattern("yyyy-MM-dd HH:mm:ss");
        return jfr;
    }

    @Bean
    public JdbcHelper jdbcHelper(JFinalViewResolver jfinalViewResolver, JdbcTemplate jdbcTemplate) {
        return new JdbcHelper(jfinalViewResolver, jdbcTemplate);
    }
}

package com.lhcz.service.impl;

import com.lhcz.helper.JdbcHelper;
import com.lhcz.jfinal.kit.Kv;
import com.lhcz.service.JdbcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dy.huang
 * @date 2020/9/27 23:01
 */
@Service
public class JdbcServiceImpl implements JdbcService {

    @Autowired
    JdbcHelper jdbcHelper;

    @Override
    public Object search() {
//        Map<String, Object> map = new HashMap<>();
//        map.put("id", 1);

        Kv para = Kv.by("id", 1);
        List<Map<String, Object>> list = jdbcHelper.getSqlList("sys_user", para);
        System.out.println("list = " + list);
        return list;
    }
}

package com.lhcz.controllers;

import com.lhcz.service.JdbcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dy.huang
 * @date 2020/9/29 22:33
 */
@RestController
public class SysUserController {


    @Autowired
    JdbcService jdbcService;

    @GetMapping
    public ResponseEntity getSys() {
        return new ResponseEntity<>(jdbcService.search(), HttpStatus.OK);
    }
}

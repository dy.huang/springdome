package com.dy.vueadminservice.exception;

/**
 * @author huangdeyao
 * @date 2019/2/5 23:14
 */
public class AppSecretException extends RuntimeException {

    public AppSecretException(String msg) {
        super(msg);
    }
}

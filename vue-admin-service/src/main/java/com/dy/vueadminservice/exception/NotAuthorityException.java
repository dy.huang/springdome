package com.dy.vueadminservice.exception;


/**
 * 没有权限
 * @author huangdeyao
 */
public class NotAuthorityException extends RuntimeException{

    public NotAuthorityException() {
        this("没有权限！");
    }

    public NotAuthorityException(String message) {
        super(message);
    }
}

package com.dy.vueadminservice.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 *
 * @author huangdeyao
 */
@JsonSerialize(using = OAuthExceptionJacksonSerializer.class)
public class OAuth2Exception extends org.springframework.security.oauth2.common.exceptions.OAuth2Exception {



    public OAuth2Exception(String msg, Throwable t) {
        super(msg, t);

    }

    public OAuth2Exception(String msg) {
        super(msg);

    }


}

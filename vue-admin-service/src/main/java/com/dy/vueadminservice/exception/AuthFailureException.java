package com.dy.vueadminservice.exception;


/**
 * 认证失败
 * @author huangdeyao
 */
public class AuthFailureException extends RuntimeException {

    public AuthFailureException() {
        this("认证失败！");
    }

    public AuthFailureException(String message) {
        super(message);
    }
}

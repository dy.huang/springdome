package com.dy.vueadminservice.dao;

import com.dy.vueadminservice.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author huangdeyao
 * @date 2019/8/18 22:24
 */
@Repository
public interface UserDao extends JpaRepository<User, Long> {
    /**
     * 通过用户名获取用户信息
     *
     * @param usrname 用户名
     * @return
     */
    User findByUsername(String usrname);
}

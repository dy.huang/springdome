package com.dy.vueadminservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

/**
 * @author huangdeyao
 * @date 2019/2/6 22:13
 */
@Configuration
public class TokenStoreConfig {

    @Bean
    public TokenStore tokenStore() throws Exception {
        TokenStore store = new InMemoryTokenStore();
        return store;
    }
}

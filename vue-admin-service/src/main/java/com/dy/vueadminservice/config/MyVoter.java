package com.dy.vueadminservice.config;

import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.persistence.Entity;
import java.util.Collection;

@Service
public class MyVoter implements AccessDecisionVoter<Entity> {

    @Override
    public boolean supports(ConfigAttribute attribute) {
        return false;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }


    @Override
    public int vote(Authentication authentication, Entity someEntity,Collection<ConfigAttribute> config) {
        return ACCESS_GRANTED;
    }
}

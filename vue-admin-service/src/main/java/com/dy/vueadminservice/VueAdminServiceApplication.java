package com.dy.vueadminservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author huangdeyao
 */
@EnableAsync
@SpringBootApplication
@EnableTransactionManagement
@EnableJpaAuditing
public class VueAdminServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(VueAdminServiceApplication.class, args);
    }

}

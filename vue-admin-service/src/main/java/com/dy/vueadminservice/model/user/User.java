package com.dy.vueadminservice.model.user;

import com.dy.vueadminservice.model.base.BaseModel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Objects;
import java.util.Set;

/**
 * @author huangdeyao
 * @date 2019/8/16 15:57
 */
@Entity
@Getter
@Setter
@Table
public class User extends BaseModel {

    /**
     * @NotNull：不能为null，但可以为empty
     * @NotEmpty：不能为null，而且长度必须大于0
     * @NotBlank：只能作用在String上，不能为null，而且调用trim()后，长度必须大于0
     */

    /**
     * 用户名称
     */
    @NotBlank
    @Column(unique = true)
    private String username;
    /**
     * 邮箱
     */
    @NotBlank
    @Email
    private String email;
    /**
     * 电话
     */
    @NotBlank
    private String phone;
    /**
     * 密码
     */
    private String password;
    /**
     * 帐户未过期
     */
    private boolean accountNonExpired = true;
    /**
     * 帐户非锁定
     */
    private boolean accountNonLocked = true;
    /**
     * 凭证未过期
     */
    private boolean credentialsNonExpired = true;
    /**
     * 启用
     */
    private boolean enabled = false;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "UsersRoles",
            joinColumns = {@JoinColumn(name = "userId", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "roleId", referencedColumnName = "id")})
    private Set<Role> roles;

    @OneToOne
    @JoinColumn(name = "job_id")
    private Job job;
    /**
     * 部门
     */
    @OneToOne
    @JoinColumn(name = "dept_id")
    private Dept dept;

    @Transient
    private Set<String> permission;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

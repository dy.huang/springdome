package com.dy.vueadminservice.model.user;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author huangdeyao
 * @date 2019/8/18 18:34
 */
@Data
@Entity
@Table
public class RolesPermissions implements Serializable {
    /**
     * permission id
     */
    @Id
    private Long permissionId;
    /**
     * rolesId
     */
    @Id
    private Long roleId;
}

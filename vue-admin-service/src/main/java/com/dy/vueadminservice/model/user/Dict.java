package com.dy.vueadminservice.model.user;

import com.dy.vueadminservice.model.base.BaseModel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Objects;

/**
 * @author huangdeyao
 * @date 2019/8/18 22:21
 */
@Setter
@Getter
@Table
@Entity
public class Dict extends BaseModel {
    /**
     * 字典名称
     */
    @Column(name = "name", nullable = false, unique = true)
    @NotBlank
    private String name;

    /**
     * 描述
     */
    @Column(name = "remark")
    private String remark;

    @OneToMany(mappedBy = "dict", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<DictDetail> dictDetails;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Dict dict = (Dict) o;
        return Objects.equals(id, dict.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

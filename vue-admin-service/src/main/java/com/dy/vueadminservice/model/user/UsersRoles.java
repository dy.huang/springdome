package com.dy.vueadminservice.model.user;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 用户与角色关联表
 *
 * @author huangdeyao
 * @date 2019/8/16 16:17
 */
@Entity
@Data
@Table
public class UsersRoles implements Serializable {
    /**
     * user Id
     */
    @Id
    private Long userId;
    /**
     * rolesId
     */
    @Id
    private Long roleId;
}

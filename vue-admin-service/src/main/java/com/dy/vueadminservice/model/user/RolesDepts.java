package com.dy.vueadminservice.model.user;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author huangdeyao
 * @date 2019/8/18 22:10
 */
@Data
@Entity
@Table
public class RolesDepts implements Serializable {
    @Id
    private Long roleId;
    @Id
    private Long deptId;
}

package com.dy.vueadminservice.model.user;

import com.dy.vueadminservice.model.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Objects;
import java.util.Set;

/**
 * 角色
 *
 * @author huangdeyao
 * @date 2019/8/16 16:16
 */
@Entity
@Getter
@Setter
@Table
@EntityListeners(AuditingEntityListener.class)
public class Role extends BaseModel {

    @Column(nullable = false)
    @NotBlank
    private String name;
    /**
     * 数据权限类型 全部 、 本级 、 自定义
     */
    private String dataScope = "本级";
    /**
     * 数值越小，级别越大
     */
    private Integer level = 3;

    private String remark;

    @JsonIgnore
    @ManyToMany(mappedBy = "roles")
    private Set<User> users;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "RolesPermissions",
            joinColumns = {@JoinColumn(name = "roleId", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "permissionId", referencedColumnName = "id")})
    private Set<Permission> permissions;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "RolesMenus",
            joinColumns = {@JoinColumn(name = "roleId", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "menuId", referencedColumnName = "id")})
    private Set<Menu> menus;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "RolesDepts",
            joinColumns = {@JoinColumn(name = "roleId", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "deptId", referencedColumnName = "id")})
    private Set<Dept> depts;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Role role = (Role) o;
        return Objects.equals(id, role.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

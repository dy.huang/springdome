package com.dy.vueadminservice.model.user;

import com.dy.vueadminservice.model.base.BaseModel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author huangdeyao
 * @date 2019/8/18 22:21
 */
@Setter
@Getter
@Table
@Entity
public class DictDetail extends BaseModel {
    /**
     * 字典标签
     */
    @Column(name = "label", nullable = false)
    private String label;

    /**
     * 字典值
     */
    @Column(name = "value", nullable = false)
    private String value;

    /**
     * 排序
     */
    @Column(name = "sort")
    private String sort = "999";

    /**
     * 字典id
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dict_id")
    private Dict dict;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DictDetail dictDetail = (DictDetail) o;
        return Objects.equals(id, dictDetail.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

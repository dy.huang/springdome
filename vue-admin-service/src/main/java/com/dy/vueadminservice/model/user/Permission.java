package com.dy.vueadminservice.model.user;

import com.dy.vueadminservice.model.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Objects;
import java.util.Set;

/**
 * @author huangdeyao
 * @date 2019/8/18 18:10
 */
@Setter
@Getter
@Entity
@Table
public class Permission extends BaseModel {

    private String name;
    /**
     * 上级类目
     */
    private Long pid;
    private String alias;

    @JsonIgnore
    @ManyToMany(mappedBy = "permissions")
    private Set<Role> roles;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Permission permission = (Permission) o;

        return Objects.equals(id, permission.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

package com.dy.vueadminservice.constant;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author huangdeyao
 * @date 2019/3/28 13:27
 */
@Component
@Setter
@Getter
@ConfigurationProperties(prefix = "third-party.login")
public class SecurityProperties {
    /**
     * 回调地址
     */
    public String redirectUri;
    /**
     * QQ clientid
     */
    private String qqClientId;
    /**
     * QQ client-secret
     */
    private String qqClientSecret;
    /**
     * weibo clientId
     */
    private String weiboClientId;
    /**
     * weibo client-secret
     */
    private String weiboClientSecret;
}

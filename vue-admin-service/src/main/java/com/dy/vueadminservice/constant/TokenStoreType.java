package com.dy.vueadminservice.constant;

/**
 * @author huangdeyao
 */
public enum TokenStoreType {
    /*
        内存
     */
    memory,
    /*
        redis
     */
    redis,
    /*
        json web token
     */
    jwt,
    /*
        数据库
     */
    jdbc
}

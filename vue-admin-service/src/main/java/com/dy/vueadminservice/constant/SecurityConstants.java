package com.dy.vueadminservice.constant;

/**
 * 安全认证模块的常量接口类,里面包含所有要用到的常量,方便以后维护和扩展
 *
 * @author huangdeyao
 * @date 2019/2/27 15:49
 */
public class SecurityConstants {
    /**
     * 当请求需要身份认证时，默认跳转的url
     */
    public static final String DEFAULT_UNAUTHENTICATION_URL = "/authentication/require";
    /**
     * 默认的用户名密码登录请求处理url
     */
    public static final String DEFAULT_LOGIN_PROCESSING_URL_FORM = "/authentication/form";
    /**
     * 第三方登录标识
     */
    public static final String THREE_TYPE = "three_type";
//    public static final String REDIRECT_URI = "http://www.yrclubs123.com/login";
    /**
     *
     */
    public static final String REDIRECT_URI_KEY = "URL";
    /**
     * qq 认证
     */
    public static final String QQ_TYPE = "qq";
    public static final String QQ_CLIENT_ID_KEY = "APP_ID";
    /**
     *
     */
//    public static final String QQ_CLIENT_ID = "101535635";
    /**
     *
     */
    public static final String QQ_CLIENT_SECRET_KEY = "SECRET";
    /**
     *
     */
//    public static final String QQ_CLIENT_SECRET = "c1b6f6c1110423f941138b4cf31acae5";
    /**
     *
     */
    public static final String QQ_CODE = "CODE";
    /**
     * qq 认证 通过Authorization Code获取Access Token
     */
    public static final String QQ_ACCESS_TOKEN_URL = "https://graph.qq.com/oauth2.0/token?grant_type=authorization_code&" +
            "client_id=APP_ID&client_secret=SECRET&code=CODE&state=vue&redirect_uri=URL";
    /**
     * 使用Access Token来获取用户的OpenID
     */
    public static final String QQ_ACCESS_OPENID_URL = "https://graph.qq.com/oauth2.0/me?access_token=ACCESS_TOKEN";
    /**
     * 使用Access Token以及OpenID来访问和修改用户数据
     */
    public static final String QQ_ACCESS_GET_USER_INFO_URL = "https://graph.qq.com/user/get_user_info?access_token=ACCESS_TOKEN" +
            "&oauth_consumer_key=APP_ID&openid=OPENID";
    /**
     * weibo 认证
     */
    public static final String WEIBO_TYPE = "weibo";
    public static final String WEIBO_CLIENT_ID_KEY = "APP_ID";
    /**
     *
     */
//    public static final String WEIBO_CLIENT_ID = "214094284";
    /**
     *
     */
    public static final String WEIBO_CLIENT_SECRET_KEY = "SECRET";
    /**
     *
     */
//    public static final String WEIBO_CLIENT_SECRET = "98b9816663e968812fcdbf80254a1916";
    /**
     *
     */
    public static final String WEIBO_CODE = "CODE";
    public static final String WEIBO_ACCESS_TOKEN_URL = "https://api.weibo.com/oauth2/access_token?client_id=APP_ID&client_secret=SECRET&grant_type=authorization_code&redirect_uri=URL&code=CODE";
    public static final String WEIBO_GET_USER_INFO_URL = "https://api.weibo.com/2/users/show.json?access_token=ACCESS_TOKEN&uid=UID";
}

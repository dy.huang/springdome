package com.dy.vueadminservice.constant;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author huangdeyao
 * @date 2019/3/28 13:27
 */
@Component
@Setter
@Getter
@ConfigurationProperties(prefix = "app.service.oauth2")
public class Oauth2Properties {
    /**
     * token 保存位置
     * jdbc 保存方式需要創建表 參見 JdbcClientDetailsService
     */
    private TokenStoreType tokenStoreType = TokenStoreType.memory;
    /**
     * jwt 簽名
     */
    private String tokenSigningKey = "article";

/**
 * jdbc 保存token 需要導入的表
 Drop table  if exists oauth_access_token;
 create table oauth_access_token (
 create_time timestamp default now(),
 token_id VARCHAR(255),
 token BLOB,
 authentication_id VARCHAR(255),
 user_name VARCHAR(255),
 client_id VARCHAR(255),
 authentication BLOB,
 refresh_token VARCHAR(255)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 Drop table  if exists oauth_refresh_token;
 create table oauth_refresh_token (
 create_time timestamp default now(),
 token_id VARCHAR(255),
 token BLOB,
 authentication BLOB
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 */
}

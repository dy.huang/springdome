package com.dy.vueadminservice.controller;

import com.dy.vueadminservice.result.RespCode;
import com.dy.vueadminservice.result.RespEntity;
import com.dy.vueadminservice.service.users.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author TiHom
 */
@RestController
public class AppSecretController {

    /**
     * 这里有个奇怪的bug, 删掉这个定义会导致 /oauth/token 认证失败，原因目前没有找到
     */
    @Autowired
    UserInfoService userInfoService;

    @GetMapping("/me")
    public Object getMe(Authentication authentication) {
        return authentication;
    }

    @GetMapping("/info")
    public Object getInfo() {
        return userInfoService.getInfo();
    }


    @RequestMapping("/get/test")
    public RespEntity gettest(int hh) {
        int h = 10 / hh;
        return new RespEntity(RespCode.SUCCESS, h);
    }


    @RequestMapping("/test")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public RespEntity addtest(int hh) {
        int h = 10 / hh;
        return new RespEntity(RespCode.SUCCESS, h);
    }

    @RequestMapping("/user")
    @PreAuthorize("hasAnyRole('USER')")
    public RespEntity addUser(int hh) {
        int h = 10 / hh;
        return new RespEntity(RespCode.SUCCESS, h);
    }

    @RequestMapping("/all")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public RespEntity addAll(int hh) {
        int h = 10 / hh;
        return new RespEntity(RespCode.SUCCESS, h);
    }

    @RequestMapping("/common")
    public RespEntity addCommon(int hh) {
        int h = 10 / hh;
        return new RespEntity(RespCode.SUCCESS, h);
    }

    @GetMapping("/refresh")
    public RespEntity refreshPermission(HttpServletRequest request) {
        userInfoService.refreshPermission(request);
        return new RespEntity(RespCode.SUCCESS, "");
    }

    /**
     *
     * @return
     */
    @GetMapping("/vip/test")
    @PreAuthorize("hasAnyRole('VIP')")
    public String vipPath() {
        return "仅 ROLE_VIP 可看";
    }

    @GetMapping("/vip")
    public boolean updateToVIP() {
        // 得到当前的认证信息
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        //  生成当前的所有授权
        List<GrantedAuthority> updatedAuthorities = new ArrayList<>(auth.getAuthorities());
        // 添加 ROLE_VIP 授权
        updatedAuthorities.add(new SimpleGrantedAuthority("ROLE_VIP"));
        // 生成新的认证信息
        Authentication newAuth = new UsernamePasswordAuthenticationToken(auth.getPrincipal(), auth.getCredentials(), updatedAuthorities);
        // 重置认证信息
        SecurityContextHolder.getContext().setAuthentication(newAuth);
        return true;
    }
}

package com.dy.vueadminservice.service.users.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * @author huangdeyao
 * @date 2019/8/19 15:59
 */
@Data
public class UserDTO implements Serializable {

    private Long id;

    private Date createDate;
    /**
     * 用户名称
     */
    private String username;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 电话
     */
    private String phone;
    private Set<RoleSmallDTO> roles;
    private JobSmallDTO job;
    private DeptSmallDTO dept;
    private Long deptId;
    private Set<String> permission;
}

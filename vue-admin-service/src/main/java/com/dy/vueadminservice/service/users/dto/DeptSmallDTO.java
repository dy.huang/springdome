package com.dy.vueadminservice.service.users.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author huangdeyao
 */
@Data
public class DeptSmallDTO implements Serializable {

    /**
     * ID
     */
    private Long id;

    /**
     * 名称
     */
    private String name;
}

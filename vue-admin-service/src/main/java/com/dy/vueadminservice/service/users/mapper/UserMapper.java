package com.dy.vueadminservice.service.users.mapper;

import com.dy.vueadminservice.model.user.User;
import com.dy.vueadminservice.service.mapper.EntityMapper;
import com.dy.vueadminservice.service.users.dto.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Service;

/**
 * @author huangdeyao
 */
@Service
@Mapper(componentModel = "spring", uses = {RoleMapper.class, DeptMapper.class, JobMapper.class}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper extends EntityMapper<UserDTO, User> {

}

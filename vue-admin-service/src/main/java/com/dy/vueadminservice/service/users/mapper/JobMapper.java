package com.dy.vueadminservice.service.users.mapper;

import com.dy.vueadminservice.model.user.Job;
import com.dy.vueadminservice.service.mapper.EntityMapper;
import com.dy.vueadminservice.service.users.dto.JobDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",uses = {DeptMapper.class},unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface JobMapper extends EntityMapper<JobDTO, Job> {

    @Mapping(source = "deptSuperiorName", target = "deptSuperiorName")
    JobDTO toDto(Job job, String deptSuperiorName);
}

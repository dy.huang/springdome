package com.dy.vueadminservice.service.users.mapper;

import com.dy.vueadminservice.model.user.Menu;
import com.dy.vueadminservice.service.mapper.EntityMapper;
import com.dy.vueadminservice.service.users.dto.MenuDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;


@Mapper(componentModel = "spring",uses = {},unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MenuMapper extends EntityMapper<MenuDTO, Menu> {

}

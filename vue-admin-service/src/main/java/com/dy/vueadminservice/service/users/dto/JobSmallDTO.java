package com.dy.vueadminservice.service.users.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author huangdeyao
 * @date 2019/8/19 16:04
 */
@Data
public class JobSmallDTO implements Serializable {

    /**
     * ID
     */
    private Long id;

    /**
     * 名称
     */
    private String name;
}

package com.dy.vueadminservice.service.users.mapper;

import com.dy.vueadminservice.model.user.Role;
import com.dy.vueadminservice.service.mapper.EntityMapper;
import com.dy.vueadminservice.service.users.dto.RoleDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", uses = {PermissionMapper.class, MenuMapper.class, DeptMapper.class}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RoleMapper extends EntityMapper<RoleDTO, Role> {

}

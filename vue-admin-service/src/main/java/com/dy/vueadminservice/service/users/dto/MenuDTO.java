package com.dy.vueadminservice.service.users.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class MenuDTO {

    private Long id;

    private String name;

    private Long sort;

    private String path;

    private String component;

    private Long pid;

    private Boolean iFrame;

    private String icon;

    private List<MenuDTO> children;

    private Date createDate;
}

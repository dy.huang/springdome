package com.dy.vueadminservice.service.users.mapper;

import com.dy.vueadminservice.service.mapper.EntityMapper;
import com.dy.vueadminservice.model.user.Permission;
import com.dy.vueadminservice.service.users.dto.PermissionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author huangdeyao
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PermissionMapper extends EntityMapper<PermissionDTO, Permission> {

}

package com.dy.vueadminservice.service.users;

import com.dy.vueadminservice.model.user.User;
import com.dy.vueadminservice.service.users.dto.UserDTO;

import javax.servlet.http.HttpServletRequest;

/**
 * @author: huangdeyao
 * @create: 2018-09-11 11:42
 **/
public interface UserInfoService {
    /**
     * 获取登录人的信息
     *
     * @return
     */
    User findByUsername(String username);

    /**
     * 登录人信息
     *
     * @return
     */
    UserDTO getInfo();

    /**
     * 刷新权限
     */
    void refreshPermission(HttpServletRequest request);

    String userName();
}

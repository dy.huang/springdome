package com.dy.vueadminservice.service.users.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author huangdeyao
 */
@Data
public class RoleSmallDTO implements Serializable {

    private Long id;

    private String name;

    private Integer level;

    private String dataScope;
}

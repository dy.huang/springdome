package com.dy.vueadminservice.service.users.mapper;

import com.dy.vueadminservice.model.user.Dept;
import com.dy.vueadminservice.service.mapper.EntityMapper;
import com.dy.vueadminservice.service.users.dto.DeptDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author huangdeyao
 */
@Mapper(componentModel = "spring",uses = {},unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DeptMapper extends EntityMapper<DeptDTO, Dept> {

}

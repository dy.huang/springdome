package com.dy.vueadminservice.service.users.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class PermissionDTO implements Serializable {

    private Long id;

    private Date createDate;

    private String name;

    private Long pid;

    private String alias;


    private List<PermissionDTO> children;

}

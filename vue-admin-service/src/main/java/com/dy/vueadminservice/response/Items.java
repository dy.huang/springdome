package com.dy.vueadminservice.response;

import lombok.Data;

import java.util.List;


/**
 * @author huangdeyao
 */
@Data
public class Items<T> {

    long count;
    List<T> items;

    public Items() {

    }

    public Items(long count, List<T> items) {
        this.count = count;
        this.items = items;
    }


}

package com.dy.vueadminservice.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * @author huangdeyao
 */
@Getter
@Setter
public class PageAndSortResponse extends BaseResponse {

    private Integer currentPage;
    private Integer pageSize;
    private long count;
    List items;

    protected PageAndSortResponse(){}

}

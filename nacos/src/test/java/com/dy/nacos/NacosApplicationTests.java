package com.dy.nacos;

import com.alibaba.fastjson.JSON;
import com.dy.nacos.utils.HttpClientUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;


@RunWith(SpringRunner.class)
@SpringBootTest
public class NacosApplicationTests {

    private static final String APP_KEY = "deed36e8fd68bce93118a626437745fe";
    private static final String SECRET_KEY = "52c7bb90fe0764a6de67098424c334ee";
    private static final int SHOP_ID_ENTY = 810094162;
    // 沙箱环境: https://gldopenapi.keruyun.com
    //正式环境: https://openapi.keruyun.com
    //oauth2: https://open.keruyun.com
    private static final String DOMAIN_URL = "https://gldopenapi.keruyun.com";
    // 公共参数
    private static final String COMMON_PARAM = "?appKey=APP_KEY&shopIdenty=SHOP_ID_ENTY&version=1.0&timestamp=TIMESTAMP&sign=SIGN";
    private String TOKEN = "1bb9f6421aacf35a60e7c689ba293073";
    /**
     * 获取token
     * (下行接口)合作方获取门店授权对应的token
     * <p>
     * 请求描述
     * HTTP请求方式：GET
     * URI:   /open/v1/token/get
     */
    private static final String TOKEN_URL = "/open/v1/token/get";
    /**
     * 获取门店列表 HTTP请求方式：POST
     */
    private static final String SHOP_LIST_URL = "/open/v1/shop/shoplist";
    /**
     * (下行接口)顾客/会员登录
     */
    private static final String CRM_LOGIN = "/open/v1/crm/login";
    /**
     * 订单列表查询
     */
    private static final String ORDER_LIST = "/open/v1/data/order/list";
    /**
     * (下行接口)导出订单-推荐使用
     */
    private static final String ORDER_DATA = "/open/v1/data/order/export2";
    /**
     * 查询会员卡列表
     */
    private static final String GET_CUSTOMER_CARDS = "/open/v1/crm/getCustomerCards";

    /**
     * 获取token 接口
     */
    @Test
    public void getToken() throws NoSuchAlgorithmException, IOException {
        String url = urlHandle(TOKEN_URL, SECRET_KEY);
        String body = HttpClientUtils.sendGetData(url, "UTF-8");
        System.out.println("body = " + body);
    }

    /**
     * 获取门店列表
     *
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    @Test
    public void getShoplist() throws NoSuchAlgorithmException, IOException {
        System.out.println("TOKEN = " + TOKEN);
        String url = urlHandle(SHOP_LIST_URL, TOKEN);
        String body = HttpClientUtils.sendPostDataByJson(url, "", "UTF-8");
        System.out.println("body = " + body);
    }

    /**
     * 顾客/会员登录
     *
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    @Test
    public void getLogin() throws NoSuchAlgorithmException, IOException {
        System.out.println("TOKEN = " + TOKEN);
        String url = urlHandle(CRM_LOGIN, TOKEN);
        Map<String, Object> map = new HashMap<>();
//        手机号码\微信openId\座机号码\顾客ID
        map.put("loginId", "15739512651");
//        登录类型(0:手机注册客户;1:微信注册用户;2:座机号;101:微信会员卡卡号;)
        map.put("loginType", 0);
        String body = HttpClientUtils.sendPostDataByJson(url, JSON.toJSONString(map), "UTF-8");
        System.out.println("body = " + body);
    }

    /**
     * 查询会员卡列表
     *
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    @Test
    public void getCustomerCards() throws NoSuchAlgorithmException, IOException {
        System.out.println("TOKEN = " + TOKEN);
        String url = urlHandle(GET_CUSTOMER_CARDS, TOKEN);
        Map<String, Object> map = new HashMap<>();
        map.put("customerId", "8849923");
        String body = HttpClientUtils.sendPostDataByJson(url, JSON.toJSONString(map), "UTF-8");
        System.out.println("body = " + body);
    }

    /**
     * 订单列表查询
     *
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    @Test
    public void getOrderList() throws NoSuchAlgorithmException, IOException {
        System.out.println("TOKEN = " + TOKEN);
        String url = urlHandle(ORDER_LIST, TOKEN);
        Map<String, Object> map = new HashMap<>();
        map.put("startTime", "1563442414792");
        map.put("endTime", "1563442414792");
        map.put("startId", 1);
        String body = HttpClientUtils.sendPostDataByJson(url, JSON.toJSONString(map), "UTF-8");
        System.out.println("body = " + body);
    }

    /**
     * 订单列表查询
     *
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    @Test
    public void getOrderData() throws NoSuchAlgorithmException, IOException {
        System.out.println("TOKEN = " + TOKEN);
        String url = urlHandle(ORDER_DATA, TOKEN);
        Map<String, Object> map = new HashMap<>();
        map.put("shopIdenty", SHOP_ID_ENTY);
        map.put("startTime", "1563442414792");
        map.put("endTime", "1563443180286");
        String body = HttpClientUtils.sendPostDataByJson(url, JSON.toJSONString(map), "UTF-8");
        System.out.println("body = " + body);
    }


    /**
     * url 组装
     *
     * @return
     */
    private String urlHandle(String paramUrl, String paramKey) throws NoSuchAlgorithmException {
        long timestamp = System.currentTimeMillis();
        System.out.println("timestamp = " + timestamp);
        // 获取token时，传递的是SECRET_KEY，其他传token值
        String sign = paramHandle(timestamp, paramKey);
        String url = StringUtils.replaceEachRepeatedly(DOMAIN_URL + paramUrl + COMMON_PARAM, new String[]{"APP_KEY", "SHOP_ID_ENTY", "TIMESTAMP", "SIGN"}, new String[]{APP_KEY, String.valueOf(SHOP_ID_ENTY), String.valueOf(timestamp), sign});
        System.out.println("url = " + url);
        return url;
    }

    /**
     * 组装String
     *
     * @param timestamp
     * @param paramKey
     * @return
     * @throws NoSuchAlgorithmException
     */
    private String paramHandle(long timestamp, String paramKey) throws NoSuchAlgorithmException {
        Map<String, Object> params = new TreeMap<>();
        params.put("appKey", APP_KEY);
        params.put("shopIdenty", SHOP_ID_ENTY);
        params.put("version", "1.0");
        params.put("timestamp", timestamp);
        StringBuilder sortedParams = new StringBuilder();
        params.entrySet().stream().forEachOrdered(paramEntry -> sortedParams.append(paramEntry.getKey()).append(paramEntry.getValue()));
        //请替换成真实的secretKey
        sortedParams.append(paramKey);
        return getSign(sortedParams.toString());
    }

    /**
     * @param
     * @return String
     * @throws NoSuchAlgorithmException
     * @Description: SHA256加密字符串
     */
    private static String getSign(String sortedParams) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(sortedParams.getBytes());
        byte byteBuffer[] = messageDigest.digest();
        StringBuffer strHexString = new StringBuffer();
        for (int i = 0; i < byteBuffer.length; i++) {
            String hex = Integer.toHexString(0xff & byteBuffer[i]);
            if (hex.length() == 1) {
                strHexString.append('0');
            }
            strHexString.append(hex);
        }
        // 得到返回結果
        String SHA256Sign = strHexString.toString();
        return SHA256Sign;
    }
}

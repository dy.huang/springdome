//package com.dy.nacos.utils;
//
//import lombok.extern.log4j.Log4j2;
//import org.apache.commons.lang.StringUtils;
//
//import java.security.MessageDigest;
//import java.security.NoSuchAlgorithmException;
//import java.util.Map;
//import java.util.TreeMap;
//
///**
// * @author huangdeyao
// * @date 2019/7/18 16:00
// */
//@Log4j2
//public class keruyunExample {
//    /**
//     * @param
//     * @throws
//     * @Description: 获取token时签名验证（只在获取token时调用一次）
//     */
//    public static void main(String[] args) {
//        String appKey = "deed36e8fd68bce93118a626437745fe";
//        String secretKey = "52c7bb90fe0764a6de67098424c334ee";
//        int shopIdenty = 810094162;
//        long timestamp = System.currentTimeMillis();
//        System.out.println("timestamp = " + timestamp);
//        Map<String, Object> params = new TreeMap<>();
//        params.put("appKey", appKey);
//        params.put("shopIdenty", shopIdenty);
//        params.put("version", "1.0");
//        params.put("timestamp", timestamp);
//        StringBuilder sortedParams = new StringBuilder();
//        params.entrySet().stream().forEachOrdered(paramEntry -> sortedParams.append(paramEntry.getKey()).append(paramEntry.getValue()));
//        //请替换成真实的secretKey
//        sortedParams.append(secretKey);
//
//        String SHA256Sign = null;
//        try {
//            SHA256Sign = getSign(sortedParams.toString());
//            System.out.println("SHA256Sign = " + SHA256Sign);
//        } catch (NoSuchAlgorithmException e) {
//            log.info("获取签名出错" + e.getMessage(), e);
//        }
//    }
//
//    /**
//     * 普通接口加密，获取到token之后
//     **/
////    public static void main(String[] args) {
////        Map<String, Object> params = new TreeMap<>();
////        params.put("appKey", "301001");
////        params.put("shopIdenty", 247900001);
////        params.put("version", "1.0");
////        params.put("timestamp", 1425635264);
////        StringBuilder sortedParams = new StringBuilder();
////        params.entrySet().stream().forEachOrdered(paramEntry -> sortedParams.append(paramEntry.getKey()).append(paramEntry.getValue()));
////        sortedParams.append("TOKEN");//请替换成真实的token
////        System.out.println(sortedParams);
////        try {
////            String sign = getSign(sortedParams.toString());
////            System.out.println(sign + "       " + sign.length());
////        } catch (NoSuchAlgorithmException e) {
////            e.printStackTrace();
////        }
////    }
//
//    /**
//     * @param
//     * @return String
//     * @throws NoSuchAlgorithmException
//     * @Description: SHA256加密字符串
//     */
//    private static String getSign(String sortedParams) throws NoSuchAlgorithmException {
//        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
//        messageDigest.update(sortedParams.getBytes());
//        byte byteBuffer[] = messageDigest.digest();
//        StringBuffer strHexString = new StringBuffer();
//        for (int i = 0; i < byteBuffer.length; i++) {
//            String hex = Integer.toHexString(0xff & byteBuffer[i]);
//            if (hex.length() == 1) {
//                strHexString.append('0');
//            }
//            strHexString.append(hex);
//        }
//        // 得到返回結果
//        String SHA256Sign = strHexString.toString();
//        return SHA256Sign;
//    }
//
//}

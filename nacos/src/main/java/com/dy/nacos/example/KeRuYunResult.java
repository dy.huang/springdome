package com.dy.nacos.example;

/**
 * @author huangdeyao
 * @date 2019/7/18 16:51
 */
public class KeRuYunResult {

    /**
     * result : {"token":"1bb9f6421aacf35a60e7c689ba293073"}
     * code : 0
     * message : 成功[OK]
     * messageUuid : 6822d53c6d164cfb8c2c9febb928ea35
     * apiMessage : null
     */

    private ResultBean result;
    private int code;
    private String message;
    private String messageUuid;
    private Object apiMessage;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageUuid() {
        return messageUuid;
    }

    public void setMessageUuid(String messageUuid) {
        this.messageUuid = messageUuid;
    }

    public Object getApiMessage() {
        return apiMessage;
    }

    public void setApiMessage(Object apiMessage) {
        this.apiMessage = apiMessage;
    }

    public static class ResultBean {
        /**
         * token : 1bb9f6421aacf35a60e7c689ba293073
         */

        private String token;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }
}

package com.dy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author hdy
 */
@SpringBootApplication
public class KafkaWebsocketApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaWebsocketApplication.class, args);
    }

}

//package com.dy.nacos.discovery.client.service;
//
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.web.bind.annotation.GetMapping;
//
///**
// * @author huangdeyao
// * @date 2019/7/19 18:00
// */
//@FeignClient("nacos-producer")
//public interface FeignService {
//    @GetMapping("/echo/producer")
//    String getNacosProducer();
//}

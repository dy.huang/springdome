# Spring Cloud + Nacos 三部曲之Discovery消费者

1、[Spring Cloud+Nacos 三部曲之Config](https://gitee.com/dy.huang/springdome/tree/master/nacos-config)

2、[Spring Cloud + Nacos 三部曲之Discovery服务注册发现](https://gitee.com/dy.huang/springdome/tree/master/nacos-discovery-producer)

3、[Spring Cloud + Nacos 三部曲之Discovery消费者](https://gitee.com/dy.huang/springdome/tree/master/nacos-discovery-client)

##  版本
1. springboot版本：2.1.6.RELEASE
2. nacos版本[Nacos 1.1.0](https://github.com/alibaba/nacos/releases)

## 创建一个springboot项目
#### 快速开始
1. pom引用
```xml
       <!--配置 屏蔽,方便测试-->
        <!--<dependency>-->
            <!--<groupId>org.springframework.cloud</groupId>-->
            <!--<artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>-->
            <!--<version>0.9.0.RELEASE</version>-->
        <!--</dependency>-->
        <!--服务注册-->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
            <version>0.9.0.RELEASE</version>
        </dependency>
```
2. yml配置

修改配置名称： bootstrap.yml
添加配置信息
```yml
spring:
  application:
    name: nacos-client
  cloud:
    nacos:
#      config:
#        server-addr: 127.0.0.1:8848
#        file-extension: yaml
      discovery:
        server-addr: 127.0.0.1:8848
server:
  port: 8086


```

3. 代码

a.NacosController.class
```java
@RestController
public class NacosController {

    @Autowired
    private LoadBalancerClient loadBalancerClient;
    @Autowired
    private RestTemplate restTemplate;

    @Value("${spring.application.name}")
    private String appName;

    @GetMapping("/echo/app-name")
    public String echoAppName() {
        //使用 LoadBalanceClient 和 RestTemolate 结合的方式来访问
        ServiceInstance serviceInstance = loadBalancerClient.choose("nacos-producer");
        String url = String.format("http://%s:%s/echo/%s", serviceInstance.getHost(), serviceInstance.getPort(), appName);
        System.out.println("request url:" + url);
        return restTemplate.getForObject(url, String.class);
    }

}
```
b.NacosDiscoveryClientApplication.class
```java

/**
 * @author huangdeyao
 */
@EnableDiscoveryClient
@SpringBootApplication
public class NacosDiscoveryClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(NacosDiscoveryClientApplication.class, args);
    }

    //实例化 RestTemplate 实例
    @Bean
    public RestTemplate restTemplate() {

        return new RestTemplate();
    }
}

```

分别启动NacosDiscoveryClientApplication，和NacosDiscoveryProducerApplication项目[Spring Cloud + Nacos 三部曲之Discovery服务注册发现](https://www.yrclubs.com/details?id=671)

请求接口：http://localhost:8086/echo/app-name
返回:Hello Nacos Discovery nacos-client

> 这里可以多启动几个NacosDiscoveryProducerApplication实例，一直刷请求，可以发现后台打印信息，nacos默认的负载均衡Ribbon机制为 轮询
> ```
> request url:http://192.168.177.1:8085/echo/nacos-client
> request url:http://192.168.177.1:8085/echo/nacos-client
> request url:http://192.168.177.1:9999/echo/nacos-client
> request url:http://192.168.177.1:8085/echo/nacos-client
> request url:http://192.168.177.1:9999/echo/nacos-client
> request url:http://192.168.177.1:8085/echo/nacos-client
> ```

### 关于 Nacos Starter 更多的配置项信息

更多关于 spring-cloud-starter-alibaba-nacos-discovery 的 starter 配置项如下所示:

| 配置项           | Key                                              | 默认值                       | 说明                                                         |
| ---------------- | ------------------------------------------------ | ---------------------------- | ------------------------------------------------------------ |
| `服务端地址`     | `spring.cloud.nacos.discovery.server-addr`       | `无`                         | `Nacos Server 启动监听的ip地址和端口`                        |
| `服务名`         | `spring.cloud.nacos.discovery.service`           | `${spring.application.name}` | `给当前的服务命名`                                           |
| `权重`           | `spring.cloud.nacos.discovery.weight`            | `1`                          | `取值范围 1 到 100，数值越大，权重越大`                      |
| `网卡名`         | `spring.cloud.nacos.discovery.network-interface` | `无`                         | `当IP未配置时，注册的IP为此网卡所对应的IP地址，如果此项也未配置，则默认取第一块网卡的地址` |
| `注册的IP地址`   | `spring.cloud.nacos.discovery.ip`                | `无`                         | `优先级最高`                                                 |
| `注册的端口`     | `spring.cloud.nacos.discovery.port`              | `-1`                         | `默认情况下不用配置，会自动探测`                             |
| `命名空间`       | `spring.cloud.nacos.discovery.namespace`         | `无`                         | `常用场景之一是不同环境的注册的区分隔离，例如开发测试环境和生产环境的资源（如配置、服务）隔离等。` |
| `AccessKey`      | `spring.cloud.nacos.discovery.access-key`        | `无`                         | `当要上阿里云时，阿里云上面的一个云账号名`                   |
| `SecretKey`      | `spring.cloud.nacos.discovery.secret-key`        | `无`                         | `当要上阿里云时，阿里云上面的一个云账号密码`                 |
| `Metadata`       | `spring.cloud.nacos.discovery.metadata`          | `无`                         | `使用Map格式配置，用户可以根据自己的需要自定义一些和服务相关的元数据信息` |
| `日志文件名`     | `spring.cloud.nacos.discovery.log-name`          | `无`                         |                                                              |
| `集群`           | `spring.cloud.nacos.discovery.cluster-name`      | `DEFAULT`                    | `配置成Nacos集群名称`                                        |
| `接入点`         | `spring.cloud.nacos.discovery.enpoint`           | `UTF-8`                      | `地域的某个服务的入口域名，通过此域名可以动态地拿到服务端地址` |
| `是否集成Ribbon` | `ribbon.nacos.enabled`                           | `true`                       | `一般都设置成true即可`                                       |

[代码](https://gitee.com/dy.huang/springdome/tree/master/nacos-discovery-client)
[个人站点](https://www.yrclubs.com)

reference
----

[Nacos Config Example](https://github.com/alibaba/spring-cloud-alibaba/blob/master/spring-cloud-alibaba-examples/nacos-example/nacos-config-example/readme-zh.md)

[Spring Cloud Alibaba Nacos Config](https://github.com/alibaba/spring-cloud-alibaba/blob/master/spring-cloud-alibaba-docs/src/main/asciidoc-zh/nacos-config.adoc)

[Spring Cloud Alibaba Nacos Discovery](https://github.com/alibaba/spring-cloud-alibaba/blob/3c6a71cac3c2d85429a0d1d53c587a2716e05831/spring-cloud-alibaba-docs/src/main/asciidoc-zh/nacos-discovery.adoc)


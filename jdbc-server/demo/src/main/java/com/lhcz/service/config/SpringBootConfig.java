package com.lhcz.service.config;


import com.lhcz.helper.JdbcHelper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;


/**
 * @author hdy
 */
@Configuration
public class SpringBootConfig {


    @Bean
    public JdbcHelper jdbcHelper(JdbcTemplate jdbcTemplate) {
        return new JdbcHelper(jdbcTemplate);
    }

}

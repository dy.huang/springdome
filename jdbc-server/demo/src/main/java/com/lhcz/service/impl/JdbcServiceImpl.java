package com.lhcz.service.impl;

import com.lhcz.helper.JdbcHelper;
import com.lhcz.service.JdbcService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dy.huang
 * @date 2020/9/27 23:01
 */
@Service
public class JdbcServiceImpl implements JdbcService {

    private final JdbcHelper jdbcHelper;

    public JdbcServiceImpl(JdbcHelper jdbcHelper) {
        this.jdbcHelper = jdbcHelper;
    }

    @Override
    public Object search() {
        Map<String, Object> map = new HashMap<>();
        map.put("id", 1);
        List<Map<String, Object>> list = jdbcHelper.getSqlList("sys_user", map);
        System.out.println("list = " + list);

        List<Map<String, Object>> list1 = jdbcHelper.jdbcTemplate().queryForList(jdbcHelper.getPath("sys_user", map));

        System.out.println("list1 = " + list1);
        return list;
    }

}

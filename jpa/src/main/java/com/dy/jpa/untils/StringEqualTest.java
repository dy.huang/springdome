package com.dy.jpa.untils;

public class StringEqualTest {
  
    public static void main(String[] args) {  
        String a = "Programming";  
        String b = new String("Programming");  
        String c = "Program" + "ming";  
          
        System.out.println(a == b);  
        System.out.println(a == c);  
        System.out.println(a.equals(b));  
        System.out.println(a.equals(c));  
        System.out.println(a.intern() == b.intern());  
    }


    public static class Test {
        public int aMethod() {
            int i = 0;
            i++;
            return i;
        }
        public static void main (String args[]) {
            Test test = new Test();
            test.aMethod();
            int j = test.aMethod();
            System.out.println(j);
        }
    }

}
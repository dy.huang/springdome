package com.dy.jpa.untils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Abc {
    public static void main(String[] args) {
        final String str = "abc";
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        executorService.execute(() -> System.out.println("1" + str));
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println("2" + str);
            }
        });
        final int[] nub = {1};
        final int[] num = {1};
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println("2" + str);
                System.out.println(nub[0]++);
                System.out.println(++nub[0]);
                System.out.println(++num[0]);
            }
        });
    }
}
/***
 * Java并发类库提供的线程池有哪几种？分别有什么特点？
 * 通常开发者都是利用Executors提供的通用线程池创建方法，去创建不同配置的线程池，主要区别在于不同的
 *
 * Executors目前提供了5种不同的线程池创建配置：
 *
 * 1、newCachedThreadPool（），它是用来处理大量短时间工作任务的线程池，具有几个鲜明特点：它会试图缓存线程并重用，当无缓存线程可用时，就会创建新的工作线程；如果线程闲置时间超过60秒，则被终止并移除缓存；长时间闲置时，这种线程池，不会消耗什么资源。其内部使用SynchronousQueue作为工作队列。
 *
 * 2、newFixedThreadPool（int nThreads），重用指定数目（nThreads）的线程，其背后使用的是无界的工作队列，任何时候最多有nThreads个工作线程是活动的。这意味着，如果任务数量超过了活动线程数目，将在工作队列中等待空闲线程出现；如果工作线程退出，将会有新的工作线程被创建，以补足指定数目nThreads。
 *
 * 3、newSingleThreadExecutor()，它的特点在于工作线程数目限制为1，操作一个无界的工作队列，所以它保证了所有的任务都是被顺序执行，最多会有一个任务处于活动状态，并且不予许使用者改动线程池实例，因此可以避免改变线程数目。
 *
 * 4、newSingleThreadScheduledExecutor()和newScheduledThreadPool(int corePoolSize)，创建的是个ScheduledExecutorService，可以进行定时或周期性的工作调度，区别在于单一工作线程还是多个工作线程。
 *
 * 5、newWorkStealingPool(int parallelism)，这是一个经常被人忽略的线程池，Java 8 才加入这个创建方法，其内部会构建ForkJoinPool，利用Work-Stealing算法，并行地处理任务，不保证处理顺序。
 *
 * https://www.cnblogs.com/gujiande/p/9488462.html
 */

package com.dy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FtpInApplication {

    public static void main(String[] args) {
        SpringApplication.run(FtpInApplication.class, args);
    }

}

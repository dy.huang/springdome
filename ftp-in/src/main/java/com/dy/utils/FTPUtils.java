package com.dy.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPFileFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.StringTokenizer;

/**
 * @author hdy
 */
@Slf4j
@Component
public class FTPUtils {

    /**
     * FTP的連接池
     */
    @Autowired
    public static FTPClientPool ftpClientPool;
    /**
     * FTPClient對象
     */
    public static FTPClient ftpClient;


    private static FTPUtils ftpUtils;

    @Autowired
    private FTPProperties ftpProperties;

    /**
     * 初始化設置
     * @return
     */
    @PostConstruct
    public boolean init() {
        FTPClientFactory factory = new FTPClientFactory(ftpProperties);
        ftpUtils = this;
        try {
            ftpClientPool = new FTPClientPool(factory);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    /**
     * 獲取連接對象
     * @return
     * @throws Exception
     */
    public static FTPClient getFTPClient() throws Exception {
        //初始化的時候從隊列中取出一個連接
        if (ftpClient==null) {
            synchronized (ftpClientPool) {
                ftpClient = ftpClientPool.borrowObject();
            }
        }
        return ftpClient;
    }


    /**
     * 當前命令執行完成命令完成
     * @throws IOException
     */
    public void complete() throws IOException {
        ftpClient.completePendingCommand();
    }

    /**
     * 當前線程任務處理完成，加入到隊列的最後
     * @return
     */
    public void disconnect() throws Exception {
        ftpClientPool.addObject(ftpClient);
    }

    /**
     * Description: 向FTP服務器上傳文件
     *
     * @Version1.0
     * @param remoteFile
     *            上傳到FTP服務器上的文件名
     * @param input
     *            本地文件流
     * @return 成功返回true，否則返回false
     */
    public static boolean uploadFile(String remoteFile, InputStream input)  {
        boolean result = false;
        try {
            getFTPClient();
            ftpClient.enterLocalPassiveMode();
            result = ftpClient.storeFile(remoteFile, input);
            input.close();
            ftpClient.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Description: 向FTP服務器上傳文件
     *
     * @Version1.0
     * @param remoteFile
     *            上傳到FTP服務器上的文件名
     * @param localFile
     *            本地文件
     * @return 成功返回true，否則返回false
     */
    public static boolean uploadFile(String remoteFile, String localFile){
        FileInputStream input = null;
        try {
            input = new FileInputStream(new File(localFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return uploadFile(remoteFile, input);
    }

    /**
     * 拷貝文件
     * @param fromFile
     * @param toFile
     * @return
     * @throws Exception
     */
    public boolean copyFile(String fromFile, String toFile) throws Exception {
        InputStream in=getFileInputStream(fromFile);
        getFTPClient();
        boolean flag = ftpClient.storeFile(toFile, in);
        in.close();
        return flag;
    }

    /**
     * 獲取文件輸入流
     * @param fileName
     * @return
     * @throws IOException
     */
    public static InputStream getFileInputStream(String fileName) throws Exception {
        ByteArrayOutputStream fos=new ByteArrayOutputStream();
        getFTPClient();
        ftpClient.retrieveFile(fileName, fos);
        ByteArrayInputStream in=new ByteArrayInputStream(fos.toByteArray());
        fos.close();
        return in;
    }

    /**
     * Description: 從FTP服務器下載文件
     *
     * @Version1.0
     * @return
     */
    public static boolean downFile(String remoteFile, String localFile){
        boolean result = false;
        try {
            getFTPClient();
            OutputStream os = new FileOutputStream(localFile);
            ftpClient.retrieveFile(remoteFile, os);
            ftpClient.logout();
            ftpClient.disconnect();
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 從ftp中獲取文件流
     * @param filePath
     * @return
     * @throws Exception
     */
    public static InputStream getInputStream(String filePath) throws Exception {
        getFTPClient();
        InputStream inputStream = ftpClient.retrieveFileStream(filePath);
        return inputStream;
    }

    /**
     * ftp中文件重命名
     * @param fromFile
     * @param toFile
     * @return
     * @throws Exception
     */
    public boolean rename(String fromFile,String toFile) throws Exception {
        getFTPClient();
        boolean result = ftpClient.rename(fromFile,toFile);
        return result;
    }

    /**
     * 獲取ftp目錄下的所有文件
     * @param dir
     * @return
     */
    public FTPFile[] getFiles(String dir) throws Exception {
        getFTPClient();
        FTPFile[] files = new FTPFile[0];
        try {
            files = ftpClient.listFiles(dir);
        }catch (Throwable thr){
            thr.printStackTrace();
        }
        return files;
    }

    /**
     * 獲取ftp目錄下的某種類型的文件
     * @param dir
     * @param filter
     * @return
     */
    public FTPFile[] getFiles(String dir, FTPFileFilter filter) throws Exception {
        getFTPClient();
        FTPFile[] files = new FTPFile[0];
        try {
            files = ftpClient.listFiles(dir, filter);
        }catch (Throwable thr){
            thr.printStackTrace();
        }
        return files;
    }

    /**
     * 創建文件夾
     * @param remoteDir
     * @return 如果已經有這個文件夾返回false
     */
    public boolean makeDirectory(String remoteDir) throws Exception {
        getFTPClient();
        boolean result = false;
        try {
            result = ftpClient.makeDirectory(remoteDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean mkdirs(String dir) throws Exception {
        boolean result = false;
        if (null == dir) {
            return result;
        }
        getFTPClient();
        ftpClient.changeWorkingDirectory("/");
        StringTokenizer dirs = new StringTokenizer(dir, "/");
        String temp = null;
        while (dirs.hasMoreElements()) {
            temp = dirs.nextElement().toString();
            //創建目錄
            ftpClient.makeDirectory(temp);
            //進入目錄
            ftpClient.changeWorkingDirectory(temp);
            result = true;
        }
        ftpClient.changeWorkingDirectory("/");
        return result;
    }
}

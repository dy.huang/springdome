package com.dy;

import com.dy.utils.FTPUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FtpInApplicationTests {

    @Test
    public void contextLoads() {
    }
    /**
     * 測試上傳
     */
    @Test
    public void uploadFile(){
        boolean flag = FTPUtils.uploadFile("hrabbit.jpg", "C:\\21\\111.txt");
        Assert.assertEquals(true, flag);
    }
}

package com.dy.config;//package com.lc.websocket;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Component;
//
//import javax.websocket.*;
//import javax.websocket.server.PathParam;
//import javax.websocket.server.ServerEndpoint;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.concurrent.CopyOnWriteArraySet;
//
//
///**
// * 接口路径 ws://localhost:8087/webSocket/userId;
// */
//@Component
//@ServerEndpoint("/websocket/{userId}")
//public class WebSocket {
//
//    private Logger logger = LoggerFactory.getLogger(getClass());
//    /**
//     * 与某个客户端的连接会话，需要通过它来给客户端发送数据
//     */
//    private Session session;
//    /**
//     * concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。
//     * 虽然@Component默认是单例模式的，但springboot还是会为每个websocket连接初始化一个bean，所以可以用一个静态set保存起来。
//     */
//    private static final CopyOnWriteArraySet<WebSocket> webSockets = new CopyOnWriteArraySet<>();
//    /**
//     * 用来存在线连接数
//     */
//    private static final Map<String, Session> sessionPool = new HashMap<>();
//
//    /**
//     * 链接成功调用的方法
//     */
//    @OnOpen
//    public void onOpen(Session session, @PathParam(value = "userId") String userId) {
//        try {
//            this.session = session;
//            webSockets.add(this);
//            sessionPool.put(userId, session);
//            logger.info("【websocket消息】有新的连接，总数为: {}", webSockets.size());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 链接关闭调用的方法
//     */
//    @OnClose
//    public void onClose() {
//        try {
//            webSockets.remove(this);
//            logger.info("【websocket消息】连接断开，总数为: {}", webSockets.size());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 收到客户端消息后调用的方法
//     *
//     * @param message message
//     */
//    @OnMessage
//    public void onMessage(String message) {
//        logger.info("【websocket消息】收到客户端 {} 消息:{}", this.session.getPathParameters().get("userId"), message);
//    }
//
//    /**
//     * 发送错误时的处理
//     *
//     * @param session session
//     * @param error   error
//     */
//    @OnError
//    public void onError(Session session, Throwable error) {
//        logger.error("用户错误: {}, 原因: {}", this.session.getPathParameters().get("userId"), error.getMessage());
//        error.printStackTrace();
//    }
//
//    /**
//     * 广播消息
//     *
//     * @param message 消息
//     */
//    public void sendAllMessage(String message) {
//        logger.info("【websocket消息】广播消息: {}", message);
//        for (WebSocket webSocket : webSockets) {
//            try {
//                if (webSocket.session.isOpen()) {
//                    webSocket.session.getAsyncRemote().sendText(message);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    /**
//     * 此为单点消息
//     *
//     * @param userId  用户
//     * @param message 消息
//     */
//    public void sendOneMessage(String userId, String message) {
//        Session session = sessionPool.get(userId);
//        if (session != null && session.isOpen()) {
//            try {
//                logger.info("【websocket消息】 私信 {} 消息: {}", userId, message);
//                session.getAsyncRemote().sendText(message);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    /**
//     * 此为单点消息
//     *
//     * @param username 发信用户
//     * @param userId   结束人
//     * @param message  消息
//     */
//    public void sendOneMessage(String username, String userId, String message) {
//        Session session = sessionPool.get(userId);
//        if (session != null && session.isOpen()) {
//            try {
//                logger.info("【websocket消息】 {} 私信 {} 消息: {}", username, userId, message);
//                session.getAsyncRemote().sendText(message);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    /**
//     * 此为单点消息(多人)
//     *
//     * @param username 发信用户
//     * @param userIds  用户组
//     * @param message  消息
//     */
//    public void sendMoreMessage(String username, String[] userIds, String message) {
//        for (String userId : userIds) {
//            Session session = sessionPool.get(userId);
//            if (session != null && session.isOpen()) {
//                try {
//                    logger.info("【websocket消息】 {} 发群消息: {}", username, message);
//                    session.getAsyncRemote().sendText(message);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
//
//    /**
//     * 此为单点消息(多人)
//     *
//     * @param userIds 用户组
//     * @param message 消息
//     */
//    public void sendMoreMessage(String[] userIds, String message) {
//        for (String userId : userIds) {
//            Session session = sessionPool.get(userId);
//            if (session != null && session.isOpen()) {
//                try {
//                    logger.info("【websocket消息】 群消息: {}", message);
//                    session.getAsyncRemote().sendText(message);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
//
//}

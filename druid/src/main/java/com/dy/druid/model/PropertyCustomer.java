package com.dy.druid.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * @author: huangdeyao
 * @create: 2018-11-12 16:00
 **/
@Data
@Entity
@Table(name = "property_customer")
@EntityListeners(AuditingEntityListener.class)
public class PropertyCustomer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    /**
     * qq
     */
    private String hashQQ;
    /**
     * SUCCESS FAIL
     */
    private String status;
    /**
     * 评分
     */
    private String score;
    /**
     * 手机
     */
    private String hashPhone;
    /**
     * 上传时的类型
     */
    private String type;
    /**
     * 上传是否成功
     */
    private boolean upload;
    /**
     * integer 推广帐号 id，有操作权限的帐号 id，包括代理商和广告主帐号 id
     */
    private Integer accountId;
    /**
     * string 用户属性数据源名称  字段长度最小 1 字节，长度最大 32 字节
     */
    private String name;
    /**
     * 用户属性数据源描述 字段长度最小 1 字节，长度最大 128 字节
     */
    private String description;
    /**
     * 用户属性数据源 id
     */
    private Integer userPropertySetId;

    /**
     * 创建时间
     */
    @CreatedDate
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createDate;
    /**
     * 修改时间
     */
    @LastModifiedDate
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastmodifiedTime;

    /**
     * 上传文件的位置
     * String path = "C:\\tx_updata\\data.txt";
     */
    @Transient
    private String path;
}

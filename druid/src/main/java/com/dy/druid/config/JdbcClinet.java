package com.dy.druid.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import sun.java2d.DisposerRecord;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author huangdeyao
 * @date 2019/8/20 18:15
 */
@Component
public class JdbcClinet implements DisposerRecord {

    static Connection conn = null;

    private static final String url = "jdbc:mysql://localhost:3306/bigdata?user=root&password=adminroot&useUnicode=true&characterEncoding=UTF8&useSSL=false&serverTimezone=UTC";

    @Bean
    public Connection getConn() throws ClassNotFoundException, SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("成功加载MySQL驱动程序");
            // 一个Connection代表一个数据库连接
            conn = DriverManager.getConnection(url);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return conn;
    }


    @Override
    public void dispose() {

    }
}

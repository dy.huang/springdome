# Spring Cloud + Nacos 三部曲之Config 配置

1、[Spring Cloud + Nacos 三部曲之Config](https://gitee.com/dy.huang/springdome/tree/master/nacos-config)

2、[Spring Cloud + Nacos 三部曲之Discovery服务注册发现](https://gitee.com/dy.huang/springdome/tree/master/nacos-discovery-producer)

3、[Spring Cloud + Nacos 三部曲之Discovery消费者](https://gitee.com/dy.huang/springdome/tree/master/nacos-discovery-client)

##  版本
1. springboot版本：2.1.6.RELEASE
2. nacos版本[Nacos 1.1.0](https://github.com/alibaba/nacos/releases)

## 创建一个springboot项目
#### 快速开始
1. pom引用
```xml
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
            <version>0.9.0.RELEASE</version>
        </dependency>
```
2. yml配置

修改配置名称： bootstrap.yml
添加配置信息
```yml
spring:
  application:
    name: nacos-config
  cloud:
    nacos:
      config:
        server-addr: 127.0.0.1:8848
        # 如果是 .properties文件 不需要这个配置
        file-extension: yaml 
server:
  port: 8080
```

3. 代码

```java
@RestController
@RefreshScope
public class SampleController {
    @Value("${user.name:null}")
    String userName;

    @Value("${user.age:25}")
    int age;

    @Value("${current.env:null}")
    String current;

    @GetMapping("/user")
    public String simple() {
        return "Hello Nacos Config!" + "Hello " + userName + " " + age + "!" + "current:" + current;
    }
}
```

4. 在nacos页面添加配置

>   **dataID**
>   在 Nacos Config Starter 中，dataId 的拼接格式如下
>
>   ```
>   ${prefix} - ${spring.profiles.active} . ${file-extension}
>   ```
>   `prefix` 默认为 `spring.application.name` 的值，也可以通过配置项 `spring.cloud.nacos.config.prefix`来配置。
>
>   `spring.profiles.active` 即为当前环境对应的 profile，详情可以参考 Spring Boot文档
>
>   **注意，当 activeprofile 为空时，对应的连接符 - 也将不存在，dataId 的拼接格式变成 ${prefix}.${file-extension}**
>
>   `file-extension` 为配置内容的数据格式，可以通过配置项` spring.cloud.nacos.config.file-extension`来配置。 目前只支持  `properties` 类型。
>
>   **group**
>   group 默认为 DEFAULT_GROUP，可以通过 spring.cloud.nacos.config.group 配置。

```
Data ID: nacos-config.yaml (这里与yml配置spring: application: name: nacos-config 保持一致)
Group: DEFAULT_GROUP
配置内容&配置格式: YAML
user:
  name: jeker
  age: 12
```
5. 测试接口 http://localhost:8080/user
> 推荐给类添加 @RefreshScope 或 @ConfigurationProperties 注解，可以编辑nacos-config.yaml 配置值，支持配置的动态更新

**返回**

```json
Hello Nacos Config!Hello jeker 12!current:null
```

#### 支持profile粒度的配置

spring-cloud-starter-alibaba-nacos-config 在加载配置的时候，不仅仅加载了以 dataid 为 `${spring.application.name}.${file-extension:properties}` 为前缀的基础配置，还加载了dataid为 `${spring.application.name}-${profile}.${file-extension:properties}` 的基础配置。在日常开发中如果遇到多套环境下的不同配置，可以通过Spring 提供的 `${spring.profiles.active}` 这个配置项来配置。

```
spring.profiles.active=develop
```

| Note | ${spring.profiles.active} 当通过配置文件来指定时必须放在 bootstrap.yml 文件中。 |
| ---- | ------------------------------------------------------------ |

新增一个2 dataid，分别为：nacos-config-develop.yaml，nacos-config-product.yaml
内容分别为
a.
```
current:
  env: develop-env
user:
  name: develop
  age: 92
  
```
b.
```
current:
  env: product-env
user:
  name: product
  age: 92
```
分别设置配置spring.profiles.active=develop和spring.profiles.active=product然后启动项目,来测试接口返回值http://localhost:8080/user

#### 多个应用间配置共享的 Data Id

可以通过以下的方式来配置：

```
spring.cloud.nacos.config.shared-dataids=bootstrap-common.properties,all-common.properties
```


## More

#### 更多配置项

| 配置项                   | key                                       | 默认值        | 说明                                                         |
| ------------------------ | ----------------------------------------- | ------------- | ------------------------------------------------------------ |
| 服务端地址               | spring.cloud.nacos.config.server-addr     |               |                                                              |
| DataId前缀               | spring.cloud.nacos.config.prefix          |               | spring.application.name                                      |
| Group                    | spring.cloud.nacos.config.group           | DEFAULT_GROUP |                                                              |
| dataID后缀及内容文件格式 | spring.cloud.nacos.config.file-extension  | properties    | dataId的后缀，同时也是配置内容的文件格式，目前只支持 properties |
| 配置内容的编码方式       | spring.cloud.nacos.config.encode          | UTF-8         | 配置的编码                                                   |
| 获取配置的超时时间       | spring.cloud.nacos.config.timeout         | 3000          | 单位为 ms                                                    |
| 配置的命名空间           | spring.cloud.nacos.config.namespace       |               | 常用场景之一是不同环境的配置的区分隔离，例如开发测试环境和生产环境的资源隔离等。 |
| AccessKey                | spring.cloud.nacos.config.access-key      |               |                                                              |
| SecretKey                | spring.cloud.nacos.config.secret-key      |               |                                                              |
| 相对路径                 | spring.cloud.nacos.config.context-path    |               | 服务端 API 的相对路径                                        |
| 接入点                   | spring.cloud.nacos.config.endpoint        | UTF-8         | 地域的某个服务的入口域名，通过此域名可以动态地拿到服务端地址 |
| 是否开启监听和自动刷新   | spring.cloud.nacos.config.refresh.enabled | true          |                                                              |

[代码](https://gitee.com/dy.huang/springdome/tree/master/nacos-config)
[个人站点](https://www.yrclubs.com/details?id=662)

reference
----
[Spring Cloud Alibaba Nacos Config](https://github.com/alibaba/spring-cloud-alibaba/blob/master/spring-cloud-alibaba-docs/src/main/asciidoc-zh/nacos-config.adoc)

[Nacos Config Example](https://github.com/alibaba/spring-cloud-alibaba/blob/master/spring-cloud-alibaba-examples/nacos-example/nacos-config-example/readme-zh.md)

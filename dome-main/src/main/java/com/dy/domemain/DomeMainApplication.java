package com.dy.domemain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DomeMainApplication {

    public static void main(String[] args) {
        SpringApplication.run(DomeMainApplication.class, args);
    }

}

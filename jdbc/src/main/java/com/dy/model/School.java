package com.dy.model;

import com.dy.model.base.BaseModel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

/**
 * @author dy.huang
 * @date 2020/3/22 20:36
 */
@Entity
@Getter
@Setter
@Table
public class School extends BaseModel {

    private long pid;
    private String schoolName;
    private String gradeName;
    private String className;
    private int num;

    @Transient
    private List<School> children;
}

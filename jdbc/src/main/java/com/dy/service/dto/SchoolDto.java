package com.dy.service.dto;

import lombok.Data;

/**
 * @author dy.huang
 * @date 2020/3/22 22:57
 */
@Data
public class SchoolDto {

    private String gradeName;
}

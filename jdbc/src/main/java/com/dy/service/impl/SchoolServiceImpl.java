package com.dy.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.dy.model.School;
import com.dy.service.SchoolService;
import com.dy.service.dto.SchoolDto;
import com.dy.utils.*;
import com.jfinal.template.ext.spring.JFinalViewResolver;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.w3c.dom.ls.LSException;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dy.huang
 * @date 2020/3/22 20:50
 */
@Service
public class SchoolServiceImpl implements SchoolService {

    private final JFinalViewResolver jFinalViewResolver;
    private final JdbcTemplate jdbcTemplate;

    public SchoolServiceImpl(JdbcTemplate jdbcTemplate, JFinalViewResolver jFinalViewResolver) {
        this.jdbcTemplate = jdbcTemplate;
        this.jFinalViewResolver = jFinalViewResolver;
    }

    @Override
    public Object getCounts() {
        String sql = "SELECT * from school WHERE grade_name = '一年级'";
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        List<Map<String, Object>> list1 = StringUtils.formatHumpNameForList(list);
        System.out.println("list1 = " + list1);
//        HashMap<String, Object> data = new HashMap<>(10);
//        data.put("gradeName", "一年级");
        SchoolDto schoolDto = new SchoolDto();
//        schoolDto.setGradeName("一年级");
        Map<String, Object> data = MapObjUtil.object2Map(schoolDto);
        String path = FileUtils.getSql("school");
        String str = jFinalViewResolver.getEngine().getTemplateByString(path).renderToString(data);
        System.out.println("str = " + str);
        List<Map<String, Object>> list2 = jdbcTemplate.queryForList(str);
        List<Map<String, Object>> list3 = StringUtils.formatHumpNameForList(list2);
        System.out.println("list3 = " + list3);
        String string = JSON.toJSONString(list3);
        System.out.println("string = " + string);
        List<School> schools = JSON.parseArray(string,School.class);
        System.out.println("schools = " + schools);
        List<School> schoolList = TreeUtils.treeMenuCountsList(schools,0);
        System.out.println("schoolList = " + schoolList);
        return list1;
    }
}

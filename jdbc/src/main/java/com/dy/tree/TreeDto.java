package com.dy.tree;

import lombok.Data;

import java.util.List;

/**
 * @author dy.huang
 * @date 2020/3/21 23:20
 */
@Data
public class TreeDto {

    private int value;
    private int pid;
    private int id;
    private String code;
    private String name;
    private List<TreeDto> children;

    public TreeDto() {
    }

    public TreeDto(int value, String code, String name, int pid, int id) {
        this.value = value;
        this.pid = pid;
        this.id = id;
        this.code = code;
        this.name = name;
    }
}


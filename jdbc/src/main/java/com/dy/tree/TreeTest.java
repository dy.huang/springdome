package com.dy.tree;



import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author dy.huang
 * @date 2020/3/21 23:23
 */
public class TreeTest {

    public static void main(String[] args) {
        List<TreeDto> list = new ArrayList<>();
        list.add(new TreeDto(1, "653131", "塔什库尔干县", 1, 8));
        list.add(new TreeDto(3, "653131203000", "提孜那甫乡", 8, 12));
        list.add(new TreeDto(3, "653131200000", "塔什库尔干乡", 8, 13));
        list.add(new TreeDto(1, "653131101000", "塔吉克阿巴提镇", 8, 11));
        list.add(new TreeDto(2, "653131101203", "胡西那瓦村", 11, 66));
        list.add(new TreeDto(2, "653131101202", "萨尔布合村", 11, 65));
        list.add(new TreeDto(2, "653131203201", "曲什曼村", 12, 62));
        list.add(new TreeDto(2, "653131203202", "提孜那甫村", 12, 60));
        list.add(new TreeDto(2, "653131203200", "栏干村", 12, 61));
        list.add(new TreeDto(1, "653131210200", "阿依克日克村", 22, 24));
        list.add(new TreeDto(2, "653131101200", "达乌来特迭村", 11, 63));
        list.add(new TreeDto(2, "653131101201", "瑙阿巴提村", 11, 64));
        list.add(new TreeDto(3, "653131101201", "瑙阿巴提村", 64, 164));
        list.add(new TreeDto(6, "653131203200", "栏干村2", 12, 61));
        list.add(new TreeDto(9, "653131203200", "栏干村12", 61, 161));
        List<TreeDto> treeDtoList = treeMenuModelList(list, 1);
        System.out.println("treeDtoList = " + treeDtoList);

    }

    /**
     * 父子递归集合 带统计功能
     *
     * @param menuList 、
     * @param parentId 第一个节点的pid
     * @return 、
     */
    public static List<TreeDto> treeMenuModelList(List<TreeDto> menuList, int parentId) {
        List<TreeDto> childMenu = new ArrayList<>();
        for (TreeDto treeDto : menuList) {
            TreeDto dto = new TreeDto();
            dto.setCode(treeDto.getCode());
            dto.setId(treeDto.getId());
            dto.setName(treeDto.getName());
            dto.setPid(treeDto.getPid());
            dto.setValue(treeDto.getValue());
            if (treeDto.getPid() == parentId) {
                List<TreeDto> tree = treeMenuModelList(menuList, treeDto.getId());
                dto.setChildren(tree);
                if (dto.getChildren().size() > 0) {
                    dto.setValue(dto.getChildren().stream().mapToInt(TreeDto::getValue).sum());
                }
                childMenu.add(dto);
            }
        }
        return childMenu;
    }

//    /**
//     * 获取所有子节点
//     *
//     * @param menuList list
//     * @param parentId 最小 pid，第一级
//     * @return 、
//     */
//    public static JSONArray treeMenuList(List<Map<String, Object>> menuList, Object parentId) {
//        JSONArray childMenu = new JSONArray();
//        for (Map<String, Object> object : menuList) {
//            JSONObject jsonMenu = JSONObject.parse(object);
//            Object menuId = jsonMenu.get("id");
//            Object pid = jsonMenu.get("pid");
//            if (parentId.toString().equals(pid.toString())) {
//                JSONArray code = treeMenuList(menuList, menuId);
//                jsonMenu.put("children", code);
//                childMenu.add(jsonMenu);
//            }
//        }
//        return childMenu;
//    }

}

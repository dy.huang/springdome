package com.dy.high.readt;

import org.junit.Test;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * 校验 结果集
 * @author hdy
 */
public class CheckResult {

/********************************************* 验证结果集代码   *****************************************************************************************************/


    /**
     * 校验代码 测试程序入口
     */
    @Test
    public void checkTest() throws IOException {
        // 开时时间
        long bTime1 = System.currentTimeMillis();
        /**
         * 主程序
         */
        checkData();
        //关闭总计时
        long eTime1 = System.currentTimeMillis();
        //输出
        System.out.println("校验代码数据共耗时：" + (eTime1 - bTime1));
    }

    /**
     * 验证结果集
     */
    private Map<String, String> tempCheckMap = new LinkedHashMap<>();

    /**
     * 读取txt 数据 检查数据点
     */
    private BufferedReader brCheck;

    {
        try {
            brCheck = new BufferedReader(new InputStreamReader(new FileInputStream(new File("C://data_input//value_map.txt")), "UTF-8"));
        } catch (UnsupportedEncodingException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 读取正确的数据结果集 与输出的结果集对比  校验是否正确
     */
    private BufferedReader brCurrent;

    {
        try {
            brCurrent = new BufferedReader(new InputStreamReader(new FileInputStream(new File("C://data_input//value_map4.txt")), "UTF-8"));
        } catch (UnsupportedEncodingException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 校验
     */
    private void checkData() throws IOException {

        System.out.println("============= 开始校验 ======================");
        long sureTime = 1493284687508L;
        try {
            String lineTxt = null;
            while ((lineTxt = brCheck.readLine()) != null) {
                // 数据切割
                String[] strings = lineTxt.split(";");
                if (strings[0] != null && !strings[0].equals("timestamp")) {
                    if (sureTime != Long.valueOf(strings[0])) {
                        System.out.println("strings[0] = " + strings[0]);
                        System.out.println("sureTime = " + sureTime);
                    } else {
                        tempCheckMap.put(strings[0], lineTxt);
                    }
                    sureTime = sureTime + 20;
                }
            }
            brCheck.close();
            checkresult(tempCheckMap);
        } catch (Exception e) {
            System.err.println("read errors :" + e);
        } finally {
            brCheck.close();
        }
    }


    /**
     * 结果比对
     */
    private void checkresult(Map<String, String> tempCheckMap) throws IOException {

        System.out.println("============= 开始校验 ======================");
        long sureTime = 1493284687508L;
        try {
            String lineTxt = null;
            while ((lineTxt = brCurrent.readLine()) != null) {
                // 数据切割
                String[] strings = lineTxt.split(";");
                if (strings[0] != null && !strings[0].equals("timestamp")) {
                    // 正确数据校验点
                    if (sureTime != Long.valueOf(strings[0])) {
                        System.out.println("strings[0] = " + strings[0]);
                        System.out.println("sureTime = " + sureTime);
                    } else {
                        String str = tempCheckMap.get(strings[0]);
                        if (null != str && !"".equals(str)) {
                            if (!str.replaceAll(" ", "").equals(lineTxt.replaceAll(" ", ""))) {
                                System.out.println("当前程序输出 =====》 " + str);
                                System.out.println("标准数据集合 =====》 " + lineTxt);
                            }
                        } else {
                            System.out.println("str = " + str);
                            System.out.println("strings[0] = " + strings[0]);
                        }
                    }
                    sureTime = sureTime + 20;
                }
            }
            brCurrent.close();
        } catch (Exception e) {
            System.err.println("read errors :" + e);
        } finally {
            brCurrent.close();
        }
    }
}

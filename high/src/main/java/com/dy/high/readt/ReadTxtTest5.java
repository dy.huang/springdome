//package com.dy.high.readt;
//
//import org.junit.Test;
//
//import java.io.*;
//import java.util.LinkedHashMap;
//import java.util.Map;
//
///**
// * @author huangdeyao
// * @date 2019/8/23 21:31
// */
//
//public class ReadTxtTest5 {
//    /**
//     * 头数据 这里使用LinkedHashMap 保证输入跟输出的顺序保持一致
//     */
//    private static Map<String, String> titleMap = new LinkedHashMap<>();
//    /**
//     * 定义一个与头信息一样的map 方便后面数据跟头数据的顺序保持一致
//     */
//    private static Map<String, String> contextMap = new LinkedHashMap<>();
//    /**
//     * 存一份缓存数据map
//     */
//    private Map<String, String> tempMap = new LinkedHashMap<>();
//    /**
//     * 基准时间
//     */
//    private long baseTime = 0;
//    /**
//     * 当前正确的游标值
//     */
//    private long correct = 0;
//    /**
//     * 执行次数, 此处理解为游标更合适
//     */
//    private long runTimes = 0;
//    /**
//     * 当前读取行号时间
//     */
//    private long nowTimes = 0;
//    /***
//     * 需要递归
//     */
//    private boolean need = false;
//    /***
//     * 是否是节点数据   当缺少1个值时候，补全一个数据后，此节点数据在信号时间点上
//     */
//    private boolean tree = false;
//    /**
//     * 读取txt 数据
//     */
//    private BufferedReader br;
//
//    {
//        try {
//            br = new BufferedReader(new InputStreamReader(new FileInputStream(new File("C://data_input//in_data.txt")), "UTF-8"));
//        } catch (UnsupportedEncodingException | FileNotFoundException e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 写入数据  Spring环境上可以考虑使用log日志方式
//     * <p>
//     * 写入数据地址
//     */
//    private BufferedWriter bw;
//
//    {
//        try {
//            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File("C://data_input//value_map.txt")), "UTF-8"));
//        } catch (UnsupportedEncodingException | FileNotFoundException e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    /**
//     * 测试程序入口
//     */
//    @Test
//    public void dealData() throws IOException {
//        // 开时时间
//        long bTime1 = System.currentTimeMillis();
//        /**
//         * 主程序
//         */
//        readTxt();
//        //关闭总计时
//        long eTime1 = System.currentTimeMillis();
//        //输出
//        System.out.println("插入数据共耗时：" + (eTime1 - bTime1));
//    }
//
//    /**
//     * 读数据
//     */
//    private void readTxt() throws IOException {
//        try {
//            String lineTxt = null;
//            while ((lineTxt = br.readLine()) != null) {
//                // 数据切割
//                String[] strings = lineTxt.split(",");
//                // 第一行头数据截取后长度大于3，其他数据集合长度截取后长度为3，此处使用该条件判断当前行数是否为头
//                if (strings.length > 3) {
//                    // 装载头信息
//                    titleHandle(strings);
//                } else {
//                    // 处理数据
//                    handleContext(strings);
//                }
//            }
//            /**
//             * 代码写入执行为n - 1;所以会遗留一条数据，这里写入最后一条数据
//             */
//            String previousContext = tempMap.get(String.valueOf(correct));
//            // 这里可以找到值，说明时间戳已经走到了下一个 这里进行写入操作
//            writerHandle(previousContext);
//            /**
//             * 关闭
//             */
//            br.close();
//            bw.close();
//        } catch (Exception e) {
//            System.err.println("read errors :" + e);
//        } finally {
//            bw.close();
//        }
//    }
//
//    /**
//     * 处理数据
//     *
//     * @param strings 切割数据
//     */
//    private void handleContext(String[] strings) throws IOException {
//        if (null != strings[0] && !"".equals(strings[0])) {
//            if ("1493284742383".equals(strings[0])) {
//                System.out.println("strings = " + strings);
//            }
//            // 第一次数据 记录基准时间
//            if (baseTime == 0) {
//                baseTime = Long.valueOf(strings[0]);
//            }
//            // 此时的信号时间点 = 基准时间  +  执行次数 * 20
//            correct = baseTime + runTimes * 20;
//            // 写入标准信号时间、
//            contextMap.put("timestamp", String.valueOf(correct));
//            // 当前时间点
//            nowTimes = Long.valueOf(strings[0]);
//            if (nowTimes == 0) {
//                nowTimes = correct;
//            }
//
//        }
//        // 将值写入map, map中的数据如果对应的编码有值，会修改，没有值继续保持之前的数据，
//        // 当前数据如果没有时间戳，此条数据会写入contextMap，不修改时间戳
//        handleMap(strings);
//        if (correct == nowTimes) {
//            // 在时间点的时候，每次写入数据 次数都需要 + 1
//            //写入缓存区
//            tempMap.put(String.valueOf(correct), handleString(contextMap));
//            if (null != strings[0] && !"".equals(strings[0])) {
//                // 只有基准时间与当前时间相等 并且有时间戳的数据才能 步进 ++
//                    runTimes++;
//            }
//        } else if (correct > nowTimes) {
//            //写入缓存区
//            tempMap.put(String.valueOf(correct), handleString(contextMap));
//        } else {
//            runTimes++;
//            // 需要递归
//            need = true;
//            // 需要判断递归大于2的情况，也就是信号数据缺失2个以上的时候，需要补多条数据
//            long loseTimes = (nowTimes - correct) / 20;
//            //小于等于1的时候，只缺一个，递归写入一行，大于1的时候，偏移还少数据，需要多次循环补全数据
//            String previousContext = null;
//            String nowTime = null;
//            // 等于1的时候 可以看做节点数据
//            if (loseTimes == 1) {
//                tree = true;
//                // 当上条数据轮空的时（correct == nowTimes） 递归会导致时间 偏差一位，时间点取不到值
//                previousContext = tempMap.get(String.valueOf(correct - 20));
//                nowTime = previousContext.replaceFirst(String.valueOf(correct - 20), String.valueOf(correct));
//                // 更新的数据
//                tempMap.put(String.valueOf(correct), nowTime);
//            } else {
//                previousContext = tempMap.get(String.valueOf(correct));
//                // 更新的数据
//                nowTime = previousContext.replaceFirst(String.valueOf(correct), String.valueOf(correct + 20));
//                // 此处需要矫正时间
//                correct = correct + 20;
//                tempMap.put(String.valueOf(correct), nowTime);
//            }
//        }
//        // 写入
//        if (tempMap.size() <= 2 && !tree) {
//            /**
//             * 取出上一条数据
//             */
//            long previousTime = correct - 20;
//            String previousContext = tempMap.get(String.valueOf(previousTime));
//            if (null != previousContext && !"".equals(previousContext)) {
//                // 这里可以找到值，说明时间戳已经走到了下一个 这里进行写入操作
//                writerHandle(previousContext);
//                tempMap.remove(String.valueOf(previousTime));
//            }
//        } else {
//            System.out.println(" =============程序执行异常，请检查数据 ========================= ");
//            System.out.println(" =============程序执行异常，请检查数据 ========================= ");
//            System.out.println("tempMap size = " + tempMap.size());
//            System.out.println(" =============程序执行异常，请检查数据 ========================= ");
//            System.out.println(" =============程序执行异常，请检查数据 ========================= ");
//            // 停止读取
//            br.close();
//            bw.close();
//        }
//
//        //是否需要递归 补全缺少的信号量数据
//        if (need) {
//            // 还原标记
//            need = false;
//            handleContext(strings);
//        }
//    }
//
//    /**
//     * 写入txt
//     */
//    private void writerHandle(String string) {
//        try {
//            bw.write(string);
//            bw.newLine();
//        } catch (Exception e) {
//            System.err.println("write errors :" + e);
//        }
//    }
//
//    /**
//     * 装载头信息
//     *
//     * @param strings 读取的当前行数据
//     */
//    private void titleHandle(String[] strings) {
//        // 初始化数据存储map的时间戳
//        contextMap.put("timestamp", "0");
//        StringBuilder title = new StringBuilder("timestamp");
//        for (String str : strings) {
//            String[] headTxt = str.split(":");
//            // 头信息存入map 用于后面便利数据
//            titleMap.put(headTxt[1], headTxt[0]);
//            // 装载一个与头信息一样的map 方便后面数据跟头数据的顺序保持一致
//            contextMap.put(headTxt[1], "0");
//            // 组装头信息
//            title.append(";").append(headTxt[1]);
//        }
//        // 写入
//        writerHandle(String.valueOf(title));
//    }
//
//    /**
//     * 将数据中的值（contextMap）与头信息（titleMap）一一对应赋值
//     * 这里的数据 n  覆盖 n-1
//     *
//     * @param strings 读取的当前行数据
//     */
//    private void handleMap(String[] strings) {
//        // 查找当前数据 相对于头数据的属性
//        for (String name : titleMap.keySet()) {
//            if (titleMap.get(name).equals(strings[1])) {
//                contextMap.put(name, strings[2]);
//                // 如果当前时间节点数据组块里面有没有重复编码数据，此处找到数据后可以使用break;提高效率
//                break;
//            }
//        }
//    }
//
//    /**
//     * 将map 中的值 组装成string
//     *
//     * @param map
//     * @return
//     */
//    private String handleString(Map<String, String> map) {
//        // 将上一个timestamp 还原，否则当前时间为n+1行的时间，没有在赋值前做是因为如果数据为最后一行，将少一条数据
//        // 组装写入txt的 string
//        StringBuilder context = new StringBuilder();
//        for (String name : map.keySet()) {
//            // 这里为了删除最后一个；
//            if ("timestamp".equals(name)) {
//                context.append(map.get(name));
//            } else {
//                context.append(";").append(map.get(name));
//            }
//        }
//        return String.valueOf(context);
//    }
//}

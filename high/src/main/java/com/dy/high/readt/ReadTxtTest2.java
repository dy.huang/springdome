//package com.dy.high.readt;
//
//import org.junit.jupiter.api.Test;
//
//import java.io.*;
//import java.util.*;
//
///**
// * @author huangdeyao
// * @date 2019/8/23 21:31
// */
//
//public class ReadTxtTest2 {
//
//
//    @Test
//    public void dealData() {
//        readTxt();
//
//    }
//
//    /**
//     * 读数据
//     */
//    private void readTxt() {
//        try {
//            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File("C://data_input//in_data.txt")), "UTF-8"));
//            String lineTxt = null;
//            // 记录集合数据
//            List<String> endData = new ArrayList<>();
//            // 头数据 这里使用LinkedHashMap 保证输入跟输出的顺序保持一致
//            Map<String, String> titleMap = new LinkedHashMap<>();
//            Map<String, String> contextMap = new LinkedHashMap<>();
//            // 保存上一次的时间
//            String timestamp = null;
//            // 第一次执行数据
//            boolean first = false;
//            while ((lineTxt = br.readLine()) != null) {
//                // 数据切割
//                String[] strings = lineTxt.split(",");
//                // 第一行头数据截取后长度大于3，其他数据集合长度截取后长度为3，此处使用该条件判断当前行数是否为头
//                if (strings.length > 3) {
//                    StringBuilder title = new StringBuilder("timestamp ");
//                    contextMap.put("timestamp", "0");
//                    for (String str : strings) {
//                        String[] headTxt = str.split(":");
//                        // 头信息存入map 用于后面便利数据
//                        titleMap.put(headTxt[1], headTxt[0]);
//                        // 装载一个与头信息一样的map 方便后面数据跟头数据的顺序保持一致
//                        contextMap.put(headTxt[1], "0");
//                        // 组装头信息
//                        title.append(" ").append(headTxt[1]);
//                    }
//                    // 头数据放入string
//                    endData.add(String.valueOf(title));
//                } else {
//
//                    // 判断是否有时间戳 每次判断有时间戳的时候初始化一次 contextMap
//                    if (null != strings[0] && !"".equals(strings[0])) {
//                        // 第一次执行
//                        if ("0".equals(contextMap.get("timestamp"))) {
//                            // 放入时间戳
//                            contextMap.put("timestamp", strings[0]);
//                            first = true;
//                        } else {
//                            // TODO: 2019/8/23
//                            //  做时间判断 20ms copy 一份数据
//                            contextMap.put("timestamp", strings[0]);
//                            long last = Long.valueOf(timestamp);
//                            long nowTime = Long.valueOf(strings[0]);
//                            long right = (nowTime - last);
//                            System.out.println("right = " + right);
//                            // 秒数延时或者时间过快（20,20） 修正秒数
//                            // 40为2个脉冲的峰值，大于40说明丢了一条脉冲信号 (有没有可能大于2个脉冲峰值？ 此处只考虑了小于或等于2个脉冲的情况), (如果2个值的时间一致整体后移？) 此处做整体后移动作
//                            if (first) {
//                                // 第一次执行
//                                endData.add(handleData(contextMap, timestamp));
//                                first = false;
//                            } else {
//                                if (right == 20) {
//                                    endData.add(handleData(contextMap, timestamp));
//                                }
//                                if (right > 20 || right < 20) {
//                                    if (right < 0 || right < 40) {
//                                        // 需要修正时间
//                                        String correct = String.valueOf(last + 20);
//                                        contextMap.put("timestamp", correct);
//                                        endData.add(handleData(contextMap, timestamp));
//                                    } else {
//                                        // 40为2个脉冲的峰值 此时为丢失数据， 需要补全数据
//                                        String correct = String.valueOf(last + 20);
//                                        contextMap.put("timestamp", correct);
//                                        endData.add(handleData(contextMap, correct));
//                                        // 此处为当前数据(表现出来的位置为 n+1条，实际是n+2 条  )直接将此条数据的时间修正;
//                                        //  如果此条脉冲信号数据的时间正好为修正后的时间，此处赋值也没有影响，所以可以不做判断, 比如补全的数据时间为20，此条数据为40
//                                        String correct1 = Long.valueOf(contextMap.get("timestamp")) + 20 + "";
//                                        contextMap.put("timestamp", correct1);
//                                        endData.add(handleData(contextMap, timestamp));
//                                    }
//                                }
//                            }
//                        }
//                        // 保存上次执行的时间
//                        timestamp = contextMap.get("timestamp");
//                        System.out.println("timestamp = " + timestamp);
//                    }
//                    // 查找当前数据 相对于头数据的属性
//                    for (String name : titleMap.keySet()) {
//                        if (titleMap.get(name).equals(strings[1])) {
//                            contextMap.put(name, strings[2]);
//                        }
//                    }
//                }
//            }
//            br.close();
//            /**
//             * 这里多写入的一条数据为 读取写入的的位置是在调取n+1 次时才add到list列表 所以会少最后一条数据
//             */
//            endData.add(handleData(contextMap, timestamp));
//            // 写入到txt
//            writerHandle(endData);
//        } catch (Exception e) {
//            System.err.println("read errors :" + e);
//        }
//    }
//
//    /**
//     * writer data for txt
//     */
//    private void writerHandle(List<String> list) {
//        try {
//            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File("C://data_input//value_map.txt")), "UTF-8"));
//            for (String str : list) {
//                bw.write(str);
//                bw.newLine();
//            }
//            bw.close();
//        } catch (Exception e) {
//            System.err.println("write errors :" + e);
//        }
//    }
//
//
//    /**
//     * 将map 中的值 组装成string
//     *
//     * @param map
//     * @param timestamp
//     * @return
//     */
//    private String handleData(Map<String, String> map, String timestamp) {
//        // 将上一个timestamp 还原，否则当前时间为n+1行的时间，没有在赋值前做是因为如果数据为最后一行，将少一条数据
//        // 组装写入txt的 string
//        StringBuilder context = new StringBuilder();
//        // 定义n 只是为了与标题对齐
//        for (String name : map.keySet()) {
//            if ("timestamp".equals(name)) {
//                context.append(timestamp).append("  ");
//            } else {
//                context.append(map.get(name)).append("  ");
//            }
//        }
//        return String.valueOf(context);
//    }
//
//
//}

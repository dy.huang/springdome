//package com.dy.high.readt;
//
//import org.junit.jupiter.api.Test;
//
//import java.io.*;
//import java.util.*;
//
///**
// * @author huangdeyao
// * @date 2019/8/23 21:31
// */
//
//public class ReadTxtTest {
//
//
//    @Test
//    public void dealData() {
//        readTxt();
//    }
//
//    /**
//     * 读数据
//     */
//    private void readTxt() {
//        try {
//            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File("C://data_input//in_data.txt")), "UTF-8"));
//            String lineTxt = null;
//            // 记录集合数据 最终写入txt集合
//            List<String> endData = new ArrayList<>();
//            // 头数据 这里使用LinkedHashMap  LinkedHashMap的特性能保证输入跟输出的顺序保持一致
//            Map<String, String> titleMap = new LinkedHashMap<>();
//            // 装载一个与头信息一样的map 方便后面数据跟头数据的顺序保持一致
//            Map<String, String> contextMap = new LinkedHashMap<>();
//            // 保存上一次的时间 n -1
//            String timestamp = null;
//            while ((lineTxt = br.readLine()) != null) {
//                // 数据切割
//                String[] strings = lineTxt.split(",");
//
//
//                // 第一行头数据截取后长度大于3，其他数据集合长度截取后长度为3，此处使用该条件判断当前行数是否为头信息
//                if (strings.length > 3) {
//                    StringBuilder title = new StringBuilder("timestamp ");
//                    // 初始化 数据时间戳 为了装载数据时判断是否是第一条数据
//                    contextMap.put("timestamp", "0");
//                    for (String str : strings) {
//                        String[] headTxt = str.split(":");
//                        // 头信息存入map 用于后面便利数据
//                        titleMap.put(headTxt[1], headTxt[0]);
//                        // 装载一个与头信息一样的map 方便后面数据跟头数据的顺序保持一致
//                        contextMap.put(headTxt[1], "0");
//                        // 组装头信息
//                        title.append(" ").append(headTxt[1]);
//                    }
//                    // 头数据放入string
//                    endData.add(String.valueOf(title));
//                } else {
//                    // 判断是否有时间戳 每次判断有时间戳的时候初始化一次 contextMap.put("timestamp", strings[0])
//                    if (null != strings[0] && !"".equals(strings[0])) {
//                        // 第一次执行写入数据操作
//                        if ("0".equals(contextMap.get("timestamp"))) {
//                            // 放入时间戳
//                            contextMap.put("timestamp", strings[0]);
//                        } else {
//                            if (timestamp == null) {
//                                // 第一次执行  这里程序运行次数为n , 当前数据为 n-1 的数据
//                                endData.add(handleData(contextMap));
//                            } else {
//                                // TODO: 2019/8/23
//                                //  做时间判断 20ms copy 一份数据
//                                long last = Long.valueOf(timestamp);
//                                long nowTime = Long.valueOf(contextMap.get("timestamp"));
//                                // 此处时间戳应该是 n-1的时间戳 -  n-1的时间戳
//                                long right = (nowTime - last);
//                                System.out.println("right = " + right);
//                                // 时间戳正常时
//                                if (right == 20) {
//                                    // 第一次执行  这里程序运行次数为n , 当前数据为 n-1 的数据
//                                    endData.add(handleData(contextMap));
//                                } else {
//                                    /**
//                                     *  计算偏移量 （20，20） 如果数据发生偏移 直接更正数据。此处没有做补全，数据计算到后面是，之间的差为负数不知道是数据原因还是理解有偏差
//                                     */
//                                    // 第一次执行  这里程序运行次数为n , 当前数据为 n-1 的数据
//                                    contextMap.put("timestamp", String.valueOf(last + 20));
//                                    endData.add(handleData(contextMap));
//                                }
//                            }
//                            // 保存 n 次执行的时间
//                            timestamp = contextMap.get("timestamp");
//                            System.out.println("timestamp = " + timestamp);
//
//                            //  写入n+1 的时间戳
//                            contextMap.put("timestamp", strings[0]);
//                        }
//
//                    }
//                    // 查找当前数据 存入相应的 头标签组 （注意利用LinkedHashMap 数据排序一致性）
//                    // map 不初始化的好处是 只有上个时间点有数据时才更新数据，否则 n + 1 的数据就与n 的值一样，与题目要求一致
//                    for (String name : titleMap.keySet()) {
//                        if (titleMap.get(name).equals(strings[1])) {
//                            contextMap.put(name, strings[2]);
//                        }
//                    }
//                }
//            }
//            br.close();
//            /**
//             * 这里多写入的一条数据为 n+1  读取n的写入的的位置是在调取n-1 次时才add到list列表 所以会少最后一条数据
//             * 此处直接更正时间，防止最后一条数据时间 有偏差
//             */
//            contextMap.put("timestamp", Long.valueOf(timestamp) + 20 + "");
//            endData.add(handleData(contextMap));
//            // 写入到txt
//            writerHandle(endData);
//        } catch (Exception e) {
//            System.err.println("read errors :" + e);
//        }
//    }
//
//    /**
//     * writer data for txt
//     */
//    private void writerHandle(List<String> list) {
//        try {
//            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File("C://data_input//value_map.txt")), "UTF-8"));
//            for (String str : list) {
//                bw.write(str);
//                bw.newLine();
//            }
//            bw.close();
//        } catch (Exception e) {
//            System.err.println("write errors :" + e);
//        }
//    }
//
//
//    /**
//     * 将map 中的值 组装成string
//     *
//     * @param map
//     * @return
//     */
//    private String handleData(Map<String, String> map) {
//        // 组装写入txt的 string
//        StringBuilder context = new StringBuilder();
//        // 装载信息,数据跟头数据的顺序保持一致
//        for (String name : map.keySet()) {
//            context.append(map.get(name)).append("  ");
//        }
//        return String.valueOf(context);
//    }
//
//
//}

package com.dy.high.lambda;

import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author huangdeyao
 * @date 2019/8/21 10:55
 */
public class LambdaTest1 {

    public static void main(String[] args) {
        test();
    }

    public static void test() {

        List<Role> roles = new ArrayList<>();
        for (int i = 2; i > 0; i--) {
            Role role = new Role();
            role.setId(i);
            role.setName("H" + i);
            roles.add(role);
        }

        List<Role> roles1 = new ArrayList<>();
        for (int i = 5; i > 0; i--) {
            Role role = new Role();
            role.setId(i);
            role.setName("H" + i);
            roles1.add(role);
        }

        List<Role> roles2 = new ArrayList<>();
        for (int i = 8; i > 0; i--) {
            Role role = new Role();
            role.setId(i);
            role.setName("H" + i);
            roles2.add(role);
        }

        List<Role> roles3 = new ArrayList<>();
        for (int i = 10; i > 0; i--) {
            Role role = new Role();
            role.setId(i);
            role.setName("H" + i);
            roles3.add(role);
        }

        List<List<Role>> list = new ArrayList<>();
        list.add(roles);
        list.add(roles1);
        list.add(roles2);
        list.add(roles3);

        System.out.println("list = " + list);

        Set<String> stringList =  list.stream().flatMap(l->l.stream()).map(Role::getName).collect(Collectors.toSet());
        stringList.stream().sorted(Comparator.comparing(String::hashCode)).collect(Collectors.toList());
        stringList.forEach(System.out::println);
    }

    @Data
    static
    class Role {
        private int id;
        private String name;
    }

}

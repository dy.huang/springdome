package com.dy.high.singleton;

/**
 * 懒汉模式-2 （线程安全），上一个(SingletonExample3)的升级版
 * 这种方式在getInstance()方法上加了同步锁，所以在多线程情况下会造成线程阻塞，把大量的线程锁在外面，只有一个线程执行完毕才会执行下一个线程。
 * @author huangdeyao
 * @date 2019/8/22 16:20
 */
public class SingletonExample4 {

    private SingletonExample4() {
    }

    private static SingletonExample4 instance = null;

    public static synchronized SingletonExample4 getInstance() {
        if (instance == null) {
            instance = new SingletonExample4();
        }
        return instance;
    }
}

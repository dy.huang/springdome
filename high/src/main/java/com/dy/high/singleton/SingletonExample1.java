package com.dy.high.singleton;

/**
 * 饿汉模式一 线程安全
 * 在类装载的时候就创建，不管你用不用先创建了再说，始终占用空间，每次调用的时候就不需要在判断，节省运行时间，浪费资源
 *
 * @author huangdeyao
 * @date 2019/8/22 10:44
 */
public class SingletonExample1 {
    /**
     * 私有构造函数
     */
    private SingletonExample1() {
    }

    private static SingletonExample1 instace = new SingletonExample1();

    public static SingletonExample1 getInstace() {
        return instace;
    }

}

package com.dy.high.singleton;

/**
 * 饿汉模式-2（线程安全）
 * 单例实例在类装载时进行创建,注意 单例对象A 和 静态模块B 的顺序
 *
 * @author huangdeyao
 * @date 2019/8/22 16:10
 */
public class SingletonExample2 {

    private SingletonExample2() {
    }

    // 单例对象 A
    private static SingletonExample2 instance = null;

    // 静态模块 B
    static {
        instance = new SingletonExample2();
    }

    public static SingletonExample2 getInstance() {
        return instance;
    }

}

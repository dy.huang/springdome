package com.dy.high.singleton;

/**
 * @author huangdeyao
 * @date 2019/8/22 16:29
 */
public class SingletonExample7 {
    private SingletonExample7() {
    }

    public static SingletonExample7 getInstance() {
        return Singleton.INSTANCE.getInstance();
    }

    /**
     * 单力模型
     */
    enum Singleton {
        /**
         * 单例模型
         */
        INSTANCE;

        private SingletonExample7 singleton = null;

        /**
         * 保证这个方法之调用异常
         */
        Singleton() {
            singleton = new SingletonExample7();
        }

        public SingletonExample7 getInstance() {
            return singleton;
        }

    }
}

package com.dy.high.singleton;

/**
 * @author huangdeyao
 * @date 2019/8/22 16:26
 */
public class SingletonExample6 {
    private SingletonExample6() {
    }

    private volatile static SingletonExample6 instance = null;

    public static SingletonExample6 getInstance() {
        if (instance == null) {
            synchronized (SingletonExample6.class) {
                if (instance == null) {
                    instance = new SingletonExample6();
                }
            }
        }
        return instance;
    }
}

package com.dy.high.singleton;

/**
 * 懒汉模式-3 (非线程安全) 双重同步锁单例模式
 * 这种模式会导致 指令重排
 * 例如：
 * 1、memory = allocate() 分配对象的内存空间
 * 2、ctorInstance() 初始化对象
 * 3、instance = memory 设置instance指向刚分配的内存
 * JVM和cpu优化，发生了指令重排，当A进入3时，B取的对象为3，B没有初始化，此时就发生了指令重排
 * 1、memory = allocate() 分配对象的内存空间
 * 3、instance = memory 设置instance指向刚分配的内存
 * 2、ctorInstance() 初始化对象
 *
 * @author huangdeyao
 * @date 2019/8/22 16:22
 */
public class SingletonExample5 {
    private SingletonExample5() {
    }

    private static SingletonExample5 intance = null;

    private static SingletonExample5 getInstance() {
        if (intance == null) {
            /**
             * 同步锁
             */
            synchronized (SingletonExample5.class) {
                if (intance == null) {
                    intance = new SingletonExample5();
                }
            }
        }
        return intance;
    }
}

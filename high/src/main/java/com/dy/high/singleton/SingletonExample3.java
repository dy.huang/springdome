package com.dy.high.singleton;

/**
 * 懒汉模式-1 (非线程安全)
 * 单例在用户第一次调用时初始化，虽然节约了资源，但是在多线程访问时，第一次出现多个调用就会产生多个实例化
 *
 * @author huangdeyao
 * @date 2019/8/22 16:18
 */
public class SingletonExample3 {
    private SingletonExample3() {
    }

    private static SingletonExample3 instance = null;

    public static SingletonExample3 getInstance() {
        if (instance == null) {
            instance = new SingletonExample3();
        }
        return instance;
    }
}

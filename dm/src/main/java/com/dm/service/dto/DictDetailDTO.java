package com.dm.service.dto;


import java.io.Serializable;


public class DictDetailDTO implements Serializable {

    private Long id;

    private String options;
    /**
     * 字典值
     */
    private String code;
    // 排序
    private String sort = "999";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}

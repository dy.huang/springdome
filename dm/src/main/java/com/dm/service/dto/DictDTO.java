package com.dm.service.dto;


import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 */
public class DictDTO implements Serializable {

    private Long id;

    private String name;

    private String remark;

    private List<DictDetailDTO> dictDetails;

    private Timestamp createTime;

    /**
     * 编码
     */
    private String code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<DictDetailDTO> getDictDetails() {
        return dictDetails;
    }

    public void setDictDetails(List<DictDetailDTO> dictDetails) {
        this.dictDetails = dictDetails;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

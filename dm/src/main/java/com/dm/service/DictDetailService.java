package com.dm.service;



import com.dm.model.DictDetail;
import com.dm.service.dto.DictDetailDTO;
import com.dm.service.dto.DictDetailQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.Map;


public interface DictDetailService {

    DictDetailDTO findById(Long id);

    DictDetailDTO create(DictDetail resources);

    void update(DictDetail resources);

    void delete(Long id);

    Map<String, Object>  queryAll(DictDetailQueryCriteria criteria, Pageable pageable);
}

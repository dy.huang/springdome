package com.dm.controllers;

import cn.hutool.core.collection.CollectionUtil;
import com.dm.model.DictDetail;
import com.dm.service.DictDetailService;
import com.dm.service.dto.DictDetailDTO;
import com.dm.service.dto.DictDetailQueryCriteria;
import com.dm.utils.BadRequestException;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@RestController
@RequestMapping("/api/dictDetail")
public class DictDetailController {

    private final DictDetailService dictDetailService;

    private static final String ENTITY_NAME = "dictDetail";

    public DictDetailController(DictDetailService dictDetailService) {
        this.dictDetailService = dictDetailService;
    }

    @GetMapping
    public ResponseEntity getDictDetails(DictDetailQueryCriteria criteria,
                                         @PageableDefault(sort = {"sort"}, direction = Sort.Direction.ASC) Pageable pageable) {
        return new ResponseEntity<>(dictDetailService.queryAll(criteria, pageable), HttpStatus.OK);
    }

    @GetMapping(value = "/map")
    public ResponseEntity getDictDetailMaps(DictDetailQueryCriteria criteria,
                                            @PageableDefault(size = 1000, sort = {"sort"}, direction = Sort.Direction.ASC) Pageable pageable) {
        String[] codes = criteria.getCode().split(",");
        Map<String, Object> map = new HashMap<>(codes.length);
        for (String code : codes) {
            criteria.setCode(code);
            map.put(code, dictDetailService.queryAll(criteria, pageable).get("content"));
        }
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @GetMapping(value = "/list")
    public ResponseEntity getDictDetailList(DictDetailQueryCriteria criteria,
                                            @PageableDefault(size = 1000, sort = {"sort"}, direction = Sort.Direction.ASC) Pageable pageable) {
        String[] codes = criteria.getCode().split(",");
        List<DictDetailDTO> list = new ArrayList<>(codes.length);
        for (String code : codes) {
            criteria.setCode(code);
            List<DictDetailDTO> object = (List<DictDetailDTO>) dictDetailService.queryAll(criteria, pageable).get("content");
            if (CollectionUtil.isNotEmpty(object)) {
                System.out.println(object);
                list.addAll(object);
            } else {
                System.out.println("code:" + code);
            }
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity create(@Validated @RequestBody DictDetail resources) {
        if (resources.getId() != null) {
            throw new BadRequestException("A new " + ENTITY_NAME + " cannot already have an ID");
        }
        return new ResponseEntity<>(dictDetailService.create(resources), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity update(@Validated(DictDetail.Update.class) @RequestBody DictDetail resources) {
        dictDetailService.update(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        dictDetailService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}

package com.dm.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
*/
@Entity
@Table(name="sys_dict")
public class Dict extends BaseModel implements Serializable {


    @NotBlank
    private String name;

    @Column(name = "remark")
    private String remark;
    /**
     * 编码
     */
    private String code;

    @OneToMany(mappedBy = "dict",cascade={CascadeType.PERSIST,CascadeType.REMOVE})
    private List<DictDetail> dictDetails;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<DictDetail> getDictDetails() {
        return dictDetails;
    }

    public void setDictDetails(List<DictDetail> dictDetails) {
        this.dictDetails = dictDetails;
    }
}

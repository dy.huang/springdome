package com.dm.model;

import javax.persistence.*;
import java.io.Serializable;

/**
*/
@Entity
@Table(name="sys_dict_item")
public class DictDetail extends BaseModel implements Serializable {

    /**
     * 字典标签
     */
    @Column(nullable = false)
    private String options;
    /**
     * 字典值
     */
    private String code;

    // 排序
    @Column(name = "sort")
    private String sort = "999";

    // 字典id
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "dict_id")
    private Dict dict;

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public Dict getDict() {
        return dict;
    }

    public void setDict(Dict dict) {
        this.dict = dict;
    }
}

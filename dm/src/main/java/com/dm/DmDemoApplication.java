package com.dm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author hdy
 */
@SpringBootApplication
public class DmDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DmDemoApplication.class, args);
    }

}

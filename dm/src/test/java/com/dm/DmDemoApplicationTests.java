package com.dm;

import com.dm.dao.DictDetailRepository;
import com.dm.dao.DictRepository;
import com.dm.model.Dict;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DmDemoApplicationTests {

    @Autowired
    DictRepository dictRepository;
    @Autowired
    DictDetailRepository dictDetailRepository;

    @Test
    public void contextLoads() {
        Pageable pageable = PageRequest.of(0, 5, Sort.Direction.DESC, "id");
        Page<Dict> list = dictRepository.findAll(pageable);
        System.out.println("list = " + list);
    }

}

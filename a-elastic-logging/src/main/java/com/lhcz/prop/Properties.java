package com.lhcz.prop;

import org.apache.http.HttpHost;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Set;

/**
 * @author hdy
 * @Description: 说明
 * @date 2020/11/23 18:14
 */
@Component
@ConfigurationProperties(prefix = "elastic.config")
public class Properties {

    /**
     * 集群地址，多个用,隔开
     */
    private Set<String> hosts;
    /**
     * 账号
     */
    private String username = "";
    /**
     * 密码
     */
    private String password = "";
    /**
     * 使用的协议
     */
    private String schema = "http";
    /**
     * 连接超时时间
     */
    private int connectTimeOut = 1000;
    /**
     * 连接超时时间
     */
    private int socketTimeOut = 30000;
    /**
     * 获取连接的超时时间
     */
    private int connectionRequestTimeOut = 500;
    /**
     * 最大连接数
     */
    private int maxConnectNum = 100;
    /**
     * 最大路由连接数
     */
    private int maxConnectPerRoute = 100;

    private ArrayList<HttpHost> hostList = new ArrayList<>();

    public ArrayList<HttpHost> getHostList() {
        for (String host : hosts) {
            String[] str = host.split(":");
            hostList.add(new HttpHost(str[0], Integer.parseInt(str[1]), schema));
        }
        return hostList;
    }


    public Set<String> getHosts() {
        return hosts;
    }

    public void setHosts(Set<String> hosts) {
        this.hosts = hosts;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public int getConnectTimeOut() {
        return connectTimeOut;
    }

    public void setConnectTimeOut(int connectTimeOut) {
        this.connectTimeOut = connectTimeOut;
    }

    public int getSocketTimeOut() {
        return socketTimeOut;
    }

    public void setSocketTimeOut(int socketTimeOut) {
        this.socketTimeOut = socketTimeOut;
    }

    public int getConnectionRequestTimeOut() {
        return connectionRequestTimeOut;
    }

    public void setConnectionRequestTimeOut(int connectionRequestTimeOut) {
        this.connectionRequestTimeOut = connectionRequestTimeOut;
    }

    public int getMaxConnectNum() {
        return maxConnectNum;
    }

    public void setMaxConnectNum(int maxConnectNum) {
        this.maxConnectNum = maxConnectNum;
    }

    public int getMaxConnectPerRoute() {
        return maxConnectPerRoute;
    }

    public void setMaxConnectPerRoute(int maxConnectPerRoute) {
        this.maxConnectPerRoute = maxConnectPerRoute;
    }

    public void setHostList(ArrayList<HttpHost> hostList) {
        this.hostList = hostList;
    }
}

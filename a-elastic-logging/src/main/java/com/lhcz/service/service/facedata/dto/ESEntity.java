package com.lhcz.service.service.facedata.dto;

import com.lhcz.model.Log;

import java.util.List;

/**
 * @author hdy
 * @Description: 说明
 * @date 2020/12/1 17:20
 */
public class ESEntity {

    /**
     * took : 0
     * timed_out : false
     * _shards : {"total":1,"successful":1,"skipped":0,"failed":0}
     * hits : {"total":{"value":5,"relation":"eq"},"max_score":1,"hits":[{"_index":"sys_log","_type":"_doc","_id":"SrSSHXYBAlmHfKIKKkBh","_score":1,"_source":{"address":"内网","browser":"Unknown","createDate":1606813842302,"description":"通过code 查询岗位","logType":"INFO","method":"com.lhcz.service.controllers.WebsocketController.findByCode()","params":"[\"123213213\"]","requestIp":"21.36.59.9","time":2,"username":"hdy"}},{"_index":"sys_log","_type":"_doc","_id":"L3-SHXYBzHEujYjDNOjX","_score":1,"_source":{"address":"内网","browser":"Unknown","createDate":1606813843992,"description":"","logType":"INFO","method":"com.lhcz.service.controllers.WebsocketController.send()","params":"[]","requestIp":"21.36.59.9","time":0,"username":"hdy"}},{"_index":"sys_log","_type":"_doc","_id":"ZQWSHXYBjFNkYdTuOXdC","_score":1,"_source":{"address":"内网","browser":"Unknown","createDate":1606813845901,"description":"修改岗位","logType":"INFO","method":"com.lhcz.service.controllers.WebsocketController.update()","params":"[{\"description\":\"登录\",\"jobCode\":\"123\",\"method\":\"1321321313\"}]","requestIp":"21.36.59.9","time":0,"username":"hdy"}},{"_index":"sys_log","_type":"_doc","_id":"S7SSHXYBAlmHfKIKPkAG","_score":1,"_source":{"address":"内网","browser":"Unknown","createDate":1606813847442,"description":"删除岗位","logType":"INFO","method":"com.lhcz.service.controllers.WebsocketController.delete()","params":"[[1,2,3,4,5]]","requestIp":"21.36.59.9","time":0,"username":"hdy"}},{"_index":"sys_log","_type":"_doc","_id":"MH-SHXYBzHEujYjDSugd","_score":1,"_source":{"address":"内网","browser":"Unknown","createDate":1606813849545,"description":"刪除","logType":"INFO","method":"com.lhcz.service.controllers.WebsocketController.getAllByCode()","params":"[\"1111111\"]","requestIp":"21.36.59.9","time":0,"username":"hdy"}}]}
     */

    private int took;
    private boolean timed_out;
    private ShardsBean _shards;
    private HitsBeanX hits;

    public int getTook() {
        return took;
    }

    public void setTook(int took) {
        this.took = took;
    }

    public boolean isTimed_out() {
        return timed_out;
    }

    public void setTimed_out(boolean timed_out) {
        this.timed_out = timed_out;
    }

    public ShardsBean get_shards() {
        return _shards;
    }

    public void set_shards(ShardsBean _shards) {
        this._shards = _shards;
    }

    public HitsBeanX getHits() {
        return hits;
    }

    public void setHits(HitsBeanX hits) {
        this.hits = hits;
    }

    public static class ShardsBean {
        /**
         * total : 1
         * successful : 1
         * skipped : 0
         * failed : 0
         */

        private int total;
        private int successful;
        private int skipped;
        private int failed;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getSuccessful() {
            return successful;
        }

        public void setSuccessful(int successful) {
            this.successful = successful;
        }

        public int getSkipped() {
            return skipped;
        }

        public void setSkipped(int skipped) {
            this.skipped = skipped;
        }

        public int getFailed() {
            return failed;
        }

        public void setFailed(int failed) {
            this.failed = failed;
        }
    }

    public static class HitsBeanX {
        /**
         * total : {"value":5,"relation":"eq"}
         * max_score : 1
         * hits : [{"_index":"sys_log","_type":"_doc","_id":"SrSSHXYBAlmHfKIKKkBh","_score":1,"_source":{"address":"内网","browser":"Unknown","createDate":1606813842302,"description":"通过code 查询岗位","logType":"INFO","method":"com.lhcz.service.controllers.WebsocketController.findByCode()","params":"[\"123213213\"]","requestIp":"21.36.59.9","time":2,"username":"hdy"}},{"_index":"sys_log","_type":"_doc","_id":"L3-SHXYBzHEujYjDNOjX","_score":1,"_source":{"address":"内网","browser":"Unknown","createDate":1606813843992,"description":"","logType":"INFO","method":"com.lhcz.service.controllers.WebsocketController.send()","params":"[]","requestIp":"21.36.59.9","time":0,"username":"hdy"}},{"_index":"sys_log","_type":"_doc","_id":"ZQWSHXYBjFNkYdTuOXdC","_score":1,"_source":{"address":"内网","browser":"Unknown","createDate":1606813845901,"description":"修改岗位","logType":"INFO","method":"com.lhcz.service.controllers.WebsocketController.update()","params":"[{\"description\":\"登录\",\"jobCode\":\"123\",\"method\":\"1321321313\"}]","requestIp":"21.36.59.9","time":0,"username":"hdy"}},{"_index":"sys_log","_type":"_doc","_id":"S7SSHXYBAlmHfKIKPkAG","_score":1,"_source":{"address":"内网","browser":"Unknown","createDate":1606813847442,"description":"删除岗位","logType":"INFO","method":"com.lhcz.service.controllers.WebsocketController.delete()","params":"[[1,2,3,4,5]]","requestIp":"21.36.59.9","time":0,"username":"hdy"}},{"_index":"sys_log","_type":"_doc","_id":"MH-SHXYBzHEujYjDSugd","_score":1,"_source":{"address":"内网","browser":"Unknown","createDate":1606813849545,"description":"刪除","logType":"INFO","method":"com.lhcz.service.controllers.WebsocketController.getAllByCode()","params":"[\"1111111\"]","requestIp":"21.36.59.9","time":0,"username":"hdy"}}]
         */

        private TotalBean total;
        private int max_score;
        private List<HitsBean> hits;

        public TotalBean getTotal() {
            return total;
        }

        public void setTotal(TotalBean total) {
            this.total = total;
        }

        public int getMax_score() {
            return max_score;
        }

        public void setMax_score(int max_score) {
            this.max_score = max_score;
        }

        public List<HitsBean> getHits() {
            return hits;
        }

        public void setHits(List<HitsBean> hits) {
            this.hits = hits;
        }

        public static class TotalBean {
            /**
             * value : 5
             * relation : eq
             */

            private int value;
            private String relation;

            public int getValue() {
                return value;
            }

            public void setValue(int value) {
                this.value = value;
            }

            public String getRelation() {
                return relation;
            }

            public void setRelation(String relation) {
                this.relation = relation;
            }
        }

        public static class HitsBean {
            /**
             * _index : sys_log
             * _type : _doc
             * _id : SrSSHXYBAlmHfKIKKkBh
             * _score : 1
             * _source : {"address":"内网","browser":"Unknown","createDate":1606813842302,"description":"通过code 查询岗位","logType":"INFO","method":"com.lhcz.service.controllers.WebsocketController.findByCode()","params":"[\"123213213\"]","requestIp":"21.36.59.9","time":2,"username":"hdy"}
             */

            private String _index;
            private String _type;
            private String _id;
            private int _score;
            private Log _source;

            public String get_index() {
                return _index;
            }

            public void set_index(String _index) {
                this._index = _index;
            }

            public String get_type() {
                return _type;
            }

            public void set_type(String _type) {
                this._type = _type;
            }

            public String get_id() {
                return _id;
            }

            public void set_id(String _id) {
                this._id = _id;
            }

            public int get_score() {
                return _score;
            }

            public void set_score(int _score) {
                this._score = _score;
            }

            public Log get_source() {
                return _source;
            }

            public void set_source(Log _source) {
                this._source = _source;
            }
        }
    }
}

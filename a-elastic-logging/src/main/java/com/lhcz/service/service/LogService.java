package com.lhcz.service.service;

import com.lhcz.model.Log;
import com.lhcz.service.service.facedata.dto.LogQueryCriteria;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.scheduling.annotation.Async;

import java.io.IOException;

/**
 *
 */
public interface LogService {

    /**
     * 分页查询
     *
     * @param criteria 查询条件
     * @return /
     */
    Object queryAll(LogQueryCriteria criteria) throws IOException;


    /**
     * 保存日志数据
     *
     * @param username  用户
     * @param browser   浏览器
     * @param ip        请求IP
     * @param joinPoint /
     * @param log       日志实体
     */
    @Async
    void save(String username, String browser, String ip, ProceedingJoinPoint joinPoint, Log log) throws IOException;


    /**
     * 查询异常详情
     *
     * @param id 日志ID
     * @return Object
     */
    Object findByErrDetail(Long id);

}

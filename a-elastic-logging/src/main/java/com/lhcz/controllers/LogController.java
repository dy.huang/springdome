package com.lhcz.controllers;

import com.lhcz.aop.log.ALog;
import com.lhcz.service.service.LogService;
import com.lhcz.service.service.facedata.dto.LogQueryCriteria;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * 日志管理
 */
@RestController
@RequestMapping("/api/logs")
public class LogController {

    private final LogService logService;

    public LogController(LogService logService) {
        this.logService = logService;
    }

    @GetMapping
    public ResponseEntity<Object> getLogs(LogQueryCriteria criteria) throws IOException {
        return new ResponseEntity<>(logService.queryAll(criteria), HttpStatus.OK);
    }

    @GetMapping(value = "/error/{id}")
    public ResponseEntity<Object> getErrorLogs(@PathVariable Long id) {
        return new ResponseEntity<>(logService.findByErrDetail(id), HttpStatus.OK);
    }

    @DeleteMapping(value = "/del/error")
    @ALog("删除所有ERROR日志")
    public ResponseEntity<Object> delAllByError() {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(value = "/del/info")
    @ALog("删除所有INFO日志")
    public ResponseEntity<Object> delAllByInfo() {
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

package com.lhcz.utils;


import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @author hdy
 * @Description: 说明
 * @date 2019/12/31 13:15
 */
public class SecurityUtils {

    /**
     * 获取系统用户名称
     *
     * @return 系统用户名称
     */
    public static String getUserDetails() {
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            return "anonymous";
        }
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            UserDetails userDetails = ((UserDetails) principal);
            return userDetails.getUsername();
        } else if (principal instanceof String) {
            return (String) principal;
        }
        return "登录状态过期";
    }

}

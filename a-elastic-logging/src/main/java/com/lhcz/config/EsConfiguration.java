package com.lhcz.config;

import com.lhcz.prop.Properties;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author hdy
 */
@Configuration
@EnableConfigurationProperties(Properties.class)
public class EsConfiguration {


    private RestClientBuilder builder;

    private final Properties properties;

    private final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();

    public EsConfiguration(Properties properties) {
        this.properties = properties;
    }


    @Bean("restHighLevelClient")
    public RestHighLevelClient client() {
        builder = RestClient.builder(properties.getHostList().toArray(new HttpHost[0]));
        setConnectTimeOutConfig();
        setMutiConnectConfig();
        return new RestHighLevelClient(builder);
    }

    /**
     * 异步httpclient的连接延时配置
     */
    private void setConnectTimeOutConfig() {
        builder.setRequestConfigCallback(requestConfigBuilder -> {
            requestConfigBuilder.setConnectTimeout(properties.getConnectTimeOut());
            requestConfigBuilder.setSocketTimeout(properties.getSocketTimeOut());
            requestConfigBuilder.setConnectionRequestTimeout(properties.getConnectionRequestTimeOut());
            return requestConfigBuilder;
        });
    }

    /**
     * 异步httpclient的连接数配置
     */
    private void setMutiConnectConfig() {
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(properties.getUsername(), properties.getPassword()));
        builder.setHttpClientConfigCallback(httpClientBuilder -> {
            httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
            httpClientBuilder.setMaxConnTotal(properties.getMaxConnectNum());
            httpClientBuilder.setMaxConnPerRoute(properties.getMaxConnectPerRoute());
            return httpClientBuilder;
        });
    }

    @Bean("restClient")
    public RestClient restClient() {
//        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(properties.getUsername(), properties.getPassword()));
        return RestClient.builder(properties.getHostList().toArray(new HttpHost[0])).setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
            @Override
            public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpAsyncClientBuilder) {
                httpAsyncClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
                httpAsyncClientBuilder.setMaxConnTotal(properties.getMaxConnectNum());
                httpAsyncClientBuilder.setMaxConnPerRoute(properties.getMaxConnectPerRoute());
                return httpAsyncClientBuilder;
            }
        }).build();
    }
}
